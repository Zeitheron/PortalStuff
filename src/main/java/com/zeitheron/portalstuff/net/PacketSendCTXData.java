package com.zeitheron.portalstuff.net;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.portalstuff.gui.inv.ContainerContextEditor;
import com.zeitheron.portalstuff.gui.ui.GuiContextEditor;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketSendCTXData implements IPacket
{
	private String name;
	private NBTTagCompound comp;
	
	static
	{
		IPacket.handle(PacketSendCTXData.class, PacketSendCTXData::new);
	}
	
	public PacketSendCTXData(String name, NBTTagCompound data)
	{
		this.name = name;
		this.comp = data;
	}
	
	public PacketSendCTXData()
	{
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setString("a", this.name);
		nbt.setTag("b", this.comp);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.name = nbt.getString("a");
		this.comp = nbt.getCompoundTag("b");
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		EntityPlayerMP player = net.getSender();
		ContainerContextEditor ctx = WorldUtil.cast(player.openContainer, ContainerContextEditor.class);
		if(ctx != null)
			ctx.accept(this, Side.SERVER);
		return null;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		GuiContextEditor editor = WorldUtil.cast(Minecraft.getMinecraft().currentScreen, GuiContextEditor.class);
		if(editor != null)
			editor.editor.accept(this, Side.CLIENT);
		return null;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public NBTTagCompound getData()
	{
		return this.comp;
	}
}