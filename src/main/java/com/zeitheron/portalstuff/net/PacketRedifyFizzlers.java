package com.zeitheron.portalstuff.net;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.portalstuff.client.FizzlerRedificator;

import net.minecraft.nbt.NBTTagCompound;

public class PacketRedifyFizzlers implements IPacket
{
	public long forTime;
	
	static
	{
		IPacket.handle(PacketRedifyFizzlers.class, PacketRedifyFizzlers::new);
	}
	
	public PacketRedifyFizzlers(long forTime)
	{
		this.forTime = forTime;
	}
	
	public PacketRedifyFizzlers()
	{
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setLong("t", this.forTime);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.forTime = nbt.getLong("t");
	}

	@Override
	public IPacket executeOnClient(PacketContext net)
	{
		FizzlerRedificator.redify(forTime);
		return null;
	}
}