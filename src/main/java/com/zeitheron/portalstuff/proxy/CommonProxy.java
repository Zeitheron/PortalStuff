package com.zeitheron.portalstuff.proxy;

public class CommonProxy
{
	public void preInit()
	{
	}
	
	public void init()
	{
	}
	
	public <T> T supplySided(String server, String client)
	{
		try
		{
			return (T) Class.forName(server).newInstance();
		} catch(Throwable throwable)
		{
			return null;
		}
	}
}