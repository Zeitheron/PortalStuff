package com.zeitheron.portalstuff.proxy;

import com.zeitheron.hammercore.api.multipart.MultipartRenderingRegistry;
import com.zeitheron.hammercore.client.render.item.ItemRenderingHandler;
import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.hammercore.proxy.RenderProxy_Client;
import com.zeitheron.portalstuff.Info;
import com.zeitheron.portalstuff.client.render.entity.RenderEntityCubeCompanion;
import com.zeitheron.portalstuff.client.render.entity.RenderEntityCubeNormal;
import com.zeitheron.portalstuff.client.render.entity.RenderEntityCubeReflection;
import com.zeitheron.portalstuff.client.render.multipart.MultipartRenderSmartWire;
import com.zeitheron.portalstuff.client.render.tile.TESRFizzler;
import com.zeitheron.portalstuff.client.render.tile.TESRFloorButtonBase;
import com.zeitheron.portalstuff.client.render.tile.TESRLaserEmitter;
import com.zeitheron.portalstuff.client.render.tile.TESRLaserFizzler;
import com.zeitheron.portalstuff.client.render.tile.TESRLightBridge;
import com.zeitheron.portalstuff.client.render.tile.TESRLightBridgePart;
import com.zeitheron.portalstuff.client.shader.RenderFizzler;
import com.zeitheron.portalstuff.entity.EntityCubeCompanion;
import com.zeitheron.portalstuff.entity.EntityCubeNormal;
import com.zeitheron.portalstuff.entity.EntityCubeReflection;
import com.zeitheron.portalstuff.init.BlocksPS;
import com.zeitheron.portalstuff.init.ItemsPS;
import com.zeitheron.portalstuff.multipart.MultipartSmartWire;
import com.zeitheron.portalstuff.tile.TileFizzler;
import com.zeitheron.portalstuff.tile.TileFizzlerPart;
import com.zeitheron.portalstuff.tile.TileFloorButtonBase;
import com.zeitheron.portalstuff.tile.TileLaserEmitter;
import com.zeitheron.portalstuff.tile.TileLaserFizzler;
import com.zeitheron.portalstuff.tile.TileLightBridge;
import com.zeitheron.portalstuff.tile.TileLightBridgePart;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.client.model.obj.OBJLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy
{
	@Override
	public void preInit()
	{
		RenderingRegistry.registerEntityRenderingHandler(EntityCubeReflection.class, RenderEntityCubeReflection.FACTORY);
		RenderingRegistry.registerEntityRenderingHandler(EntityCubeNormal.class, RenderEntityCubeNormal.FACTORY);
		RenderingRegistry.registerEntityRenderingHandler(EntityCubeCompanion.class, RenderEntityCubeCompanion.FACTORY);
		MultipartRenderingRegistry.bindSpecialMultipartRender(MultipartSmartWire.class, new MultipartRenderSmartWire());
		
		OBJLoader.INSTANCE.addDomain(Info.MOD_ID + "obj");
	}
	
	@Override
	public void init()
	{
		for(Block bl : BlocksPS.getBlocks())
			RenderProxy_Client.registerRender(Item.getItemFromBlock(bl));
		ClientRegistry.bindTileEntitySpecialRenderer(TileFizzlerPart.class, new TESRFizzler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileFizzler.class, new TESRFizzler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileLaserEmitter.class, new TESRLaserEmitter());
		ClientRegistry.bindTileEntitySpecialRenderer(TileLightBridgePart.class, new TESRLightBridgePart());
		ClientRegistry.bindTileEntitySpecialRenderer(TileFloorButtonBase.class, new TESRFloorButtonBase());
		ClientRegistry.bindTileEntitySpecialRenderer(TileLaserFizzler.class, new TESRLaserFizzler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileLightBridge.class, new TESRLightBridge());
		Minecraft.getMinecraft().getItemColors().registerItemColorHandler((stack, layer) -> 4369919, ItemsPS.SMART_WIRE);
		MinecraftForge.EVENT_BUS.register(new RenderFizzler());
	}
	
	private <T extends TileEntity> void ri(Block b, Class<T> tile, TESR<T> tesr)
	{
		ClientRegistry.bindTileEntitySpecialRenderer(tile, tesr);
		ItemRenderingHandler.INSTANCE.setItemRender(Item.getItemFromBlock(b), tesr);
	}
	
	@Override
	public <T> T supplySided(String server, String client)
	{
		return super.supplySided(client, client);
	}
}