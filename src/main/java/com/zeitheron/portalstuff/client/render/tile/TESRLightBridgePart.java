package com.zeitheron.portalstuff.client.render.tile;

import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.portalstuff.client.shader.RenderLightBridgePart;
import com.zeitheron.portalstuff.tile.TileLightBridgePart;

import net.minecraft.util.ResourceLocation;

public class TESRLightBridgePart extends TESR<TileLightBridgePart>
{
	@Override
	public void renderTileEntityAt(TileLightBridgePart te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		if(te.aabb == null)
			return;
		RenderLightBridgePart.addRenderPos(te.aabb, x, y, z);
	}
}