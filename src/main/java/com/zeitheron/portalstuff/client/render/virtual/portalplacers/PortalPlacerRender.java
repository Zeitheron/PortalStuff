package com.zeitheron.portalstuff.client.render.virtual.portalplacers;

import java.util.List;

import org.lwjgl.opengl.GL11;

import com.zeitheron.portalstuff.init.ItemsPS;
import com.zeitheron.portalstuff.virtual.portalplacers.PortalPlacer;
import com.zeitheron.portalstuff.virtual.portalplacers.PortalPlacerList;
import com.zeitheron.portalstuff.virtual.portalplacers.PortalPlacerListManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;

public class PortalPlacerRender
{
	private static final ItemStack PLACER_STACK = new ItemStack(ItemsPS.PORTAL_PLACER);
	
	public static void render()
	{
		RenderHelper.enableStandardItemLighting();
		PortalPlacerList list = PortalPlacerListManager.visiblePlacerList;
		List<PortalPlacer> placers = list.getPlacers();
		for(int i = 0; i < placers.size(); ++i)
		{
			PortalPlacer placer = placers.get(i);
			double x = (double) placer.positions[0].getX() - TileEntityRendererDispatcher.staticPlayerX;
			double y = (double) placer.positions[0].getY() - TileEntityRendererDispatcher.staticPlayerY;
			double z = (double) placer.positions[0].getZ() - TileEntityRendererDispatcher.staticPlayerZ;
			GL11.glPushMatrix();
			GL11.glTranslated((double) x, (double) (y + 1.0), (double) z);
			if(placer.placed.getAxis() == EnumFacing.Axis.Y)
			{
				if(placer.up == EnumFacing.SOUTH)
				{
					GL11.glTranslated((double) 0.0, (double) 0.0, (double) 1.0);
				}
				if(placer.up == EnumFacing.EAST)
				{
					GL11.glTranslated((double) 1.0, (double) 0.0, (double) 1.0);
					GL11.glRotated((double) 90.0, (double) 0.0, (double) 1.0, (double) 0.0);
				}
				if(placer.up == EnumFacing.WEST)
				{
					GL11.glTranslated((double) 0.0, (double) 0.0, (double) 1.0);
					GL11.glRotated((double) 90.0, (double) 0.0, (double) 1.0, (double) 0.0);
				}
				if(placer.placed == EnumFacing.UP)
				{
					GL11.glTranslated((double) 0.5, (double) -1.0, (double) 0.0);
					GL11.glRotated((double) 270.0, (double) 1.0, (double) 0.0, (double) 0.0);
				}
				if(placer.placed == EnumFacing.DOWN)
				{
					GL11.glTranslated((double) 0.5, (double) 0.0, (double) 0.0);
					GL11.glRotated((double) 90.0, (double) 1.0, (double) 0.0, (double) 0.0);
				}
			} else
			{
				if(placer.placed == EnumFacing.SOUTH)
				{
					GL11.glTranslated((double) 0.5, (double) 0.0, (double) 0.0);
				}
				if(placer.placed == EnumFacing.NORTH)
				{
					GL11.glTranslated((double) 0.5, (double) 0.0, (double) 1.0);
					GL11.glRotated((double) 180.0, (double) 0.0, (double) 1.0, (double) 0.0);
				}
				if(placer.placed == EnumFacing.EAST)
				{
					GL11.glTranslated((double) 0.0, (double) 0.0, (double) 0.5);
					GL11.glRotated((double) 90.0, (double) 0.0, (double) 1.0, (double) 0.0);
				}
				if(placer.placed == EnumFacing.WEST)
				{
					GL11.glTranslated((double) 1.0, (double) 0.0, (double) 0.5);
					GL11.glRotated((double) 270.0, (double) 0.0, (double) 1.0, (double) 0.0);
				}
			}
			GL11.glScaled((double) 1.3, (double) 2.0, (double) 0.1);
			Minecraft.getMinecraft().getRenderItem().renderItem(PLACER_STACK, ItemCameraTransforms.TransformType.FIXED);
			GL11.glPopMatrix();
		}
	}
}