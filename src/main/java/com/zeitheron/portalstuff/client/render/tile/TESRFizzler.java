package com.zeitheron.portalstuff.client.render.tile;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.portalstuff.client.shader.RenderFizzler;
import com.zeitheron.portalstuff.tile.TileFizzler;
import com.zeitheron.portalstuff.tile.TileFizzlerPart;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;

public class TESRFizzler extends TESR<TileEntity>
{
	@Override
	public void renderTileEntityAt(TileEntity te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		try
		{
			GlStateManager.enableBlend();
			if(te instanceof TileFizzler)
			{
				BlockPos hitPos;
				TileFizzler fizz = (TileFizzler) te;
				if(this.mc.objectMouseOver != null && (hitPos = this.mc.objectMouseOver.getBlockPos()) != null && hitPos.equals((Object) te.getPos()))
				{
					double s = 0.4375;
					double e = 0.5625;
					int m = ((EnumFacing) fizz.getLocation().getState().getValue(EnumRotation.EFACING)).getOpposite().ordinal() - 2;
					AxisAlignedBB baseBB = (m == 0 ? new AxisAlignedBB(s, 0.0, 0.75, e, 1.0, 1.0) : (m == 1 ? new AxisAlignedBB(s, 0.0, 0.0, e, 1.0, 0.25) : (m == 2 ? new AxisAlignedBB(0.75, 0.0, s, 1.0, 1.0, e) : new AxisAlignedBB(0.0, 0.0, s, 0.25, 1.0, e)))).grow(0.0020000000949949026);
					GlStateManager.disableTexture2D();
					GL11.glPushMatrix();
					GL11.glTranslated((double) x, (double) y, (double) z);
					RenderGlobal.drawBoundingBox((double) baseBB.minX, (double) baseBB.minY, (double) baseBB.minZ, (double) baseBB.maxX, (double) baseBB.maxY, (double) baseBB.maxZ, (float) 0.0f, (float) 0.0f, (float) 0.0f, (float) 0.4f);
					GL11.glPopMatrix();
					GlStateManager.enableTexture2D();
				}
				EnumFacing face = (EnumFacing) te.getWorld().getBlockState(te.getPos()).getValue(EnumRotation.EFACING);
				if(te.getWorld().getTileEntity(te.getPos().offset(face.getOpposite())) instanceof TileFizzlerPart)
				{
					RenderFizzler.addRenderPos(te.getPos());
				}
			} else
			{
				RenderFizzler.addRenderPos(te.getPos());
			}
		} catch(Throwable fizz)
		{
			// empty catch block
		}
	}
}