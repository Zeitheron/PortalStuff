package com.zeitheron.portalstuff.client.render.entity;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.render.vertex.SimpleBlockRendering;
import com.zeitheron.hammercore.client.utils.RenderBlocks;
import com.zeitheron.portalstuff.Info;
import com.zeitheron.portalstuff.entity.EntityCubeNormal;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.util.Mirror;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.IRenderFactory;

public class RenderEntityCubeNormal extends Render<EntityCubeNormal>
{
	public static final Factory FACTORY = new Factory();
	
	protected RenderEntityCubeNormal(RenderManager renderManager)
	{
		super(renderManager);
	}

	@Override
	public void doRender(EntityCubeNormal entity, double x, double y, double z, float entityYaw, float partialTicks)
	{
		super.doRender(entity, x, y, z, entityYaw, partialTicks);
		GL11.glPushMatrix();
		GL11.glTranslated((double) x, (double) y, (double) z);
		GL11.glRotated((double) entity.getMirroredYaw(Mirror.FRONT_BACK), (double) 0.0, (double) 1.0, (double) 0.0);
		GL11.glTranslated((double) -0.5, (double) 0.0, (double) -0.5);
		GlStateManager.enableAlpha();
		this.bindEntityTexture(entity);
		SimpleBlockRendering sbr = RenderBlocks.forMod(Info.MOD_ID).simpleRenderer;
		sbr.begin();
		sbr.setBrightness(sbr.rb.setLighting(entity.getEntityWorld(), entity.getPosition()));
		sbr.setSprite(Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(Info.MOD_ID + ":blocks/weighted_cube"));
		sbr.drawBlock(0.0, 0.0, 0.0);
		sbr.end();
		GL11.glPopMatrix();
		RenderHelper.enableStandardItemLighting();
	}

	@Override
	protected ResourceLocation getEntityTexture(EntityCubeNormal entity)
	{
		return TextureMap.LOCATION_BLOCKS_TEXTURE;
	}
	
	public static class Factory implements IRenderFactory<EntityCubeNormal>
	{
		@Override
		public Render<? super EntityCubeNormal> createRenderFor(RenderManager manager)
		{
			return new RenderEntityCubeNormal(manager);
		}
	}
}