package com.zeitheron.portalstuff.client.render.tile;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.portalstuff.init.ItemsPS;
import com.zeitheron.portalstuff.tile.TileFloorButtonBase;

import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class TESRFloorButtonBase extends TESR<TileFloorButtonBase>
{
	private static final ItemStack BASE = new ItemStack(ItemsPS.FLOOR_BUTTON_ANIMATED);

	@Override
	public void renderTileEntityAt(TileFloorButtonBase te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		float press = ((Float) te.pressure.get()).floatValue() * 1.8f;
		press = (float) Math.sin(press) * .05f;
		GL11.glPushMatrix();
		GL11.glTranslated((double) (x + 0.5), (double) (y + .65 - (double) press), (double) (z + 0.5));
		GL11.glScaled(2.5, 2.5, 2.5);
		this.mc.getRenderItem().renderItem(BASE, ItemCameraTransforms.TransformType.FIXED);
		GL11.glPopMatrix();
	}
}