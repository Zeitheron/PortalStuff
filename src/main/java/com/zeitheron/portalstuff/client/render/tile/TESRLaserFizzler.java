package com.zeitheron.portalstuff.client.render.tile;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.hammercore.client.render.vertex.SimpleBlockRendering;
import com.zeitheron.hammercore.client.utils.RenderBlocks;
import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.portalstuff.Info;
import com.zeitheron.portalstuff.blocks.BlockFizzlerPart;
import com.zeitheron.portalstuff.init.BlocksPS;
import com.zeitheron.portalstuff.tile.TileLaserFizzler;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;

public class TESRLaserFizzler extends TESR<TileEntity>
{
	@Override
	public void renderTileEntityAt(TileEntity te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		try
		{
			GlStateManager.enableBlend();
			if(te instanceof TileLaserFizzler)
			{
				BlockPos hitPos;
				TileLaserFizzler fizz = (TileLaserFizzler) te;
				if(this.mc.objectMouseOver != null && (hitPos = this.mc.objectMouseOver.getBlockPos()) != null && hitPos.equals((Object) te.getPos()))
				{
					double s = 0.4375;
					double e = 0.5625;
					int m = ((EnumFacing) fizz.getLocation().getState().getValue(EnumRotation.EFACING)).getOpposite().ordinal() - 2;
					AxisAlignedBB baseBB = (m == 0 ? new AxisAlignedBB(s, 0.0, 0.75, e, 1.0, 1.0) : (m == 1 ? new AxisAlignedBB(s, 0.0, 0.0, e, 1.0, 0.25) : (m == 2 ? new AxisAlignedBB(0.75, 0.0, s, 1.0, 1.0, e) : new AxisAlignedBB(0.0, 0.0, s, 0.25, 1.0, e)))).grow(0.0020000000949949026);
					GlStateManager.disableTexture2D();
					GL11.glPushMatrix();
					GL11.glTranslated((double) x, (double) y, (double) z);
					RenderGlobal.drawBoundingBox((double) baseBB.minX, (double) baseBB.minY, (double) baseBB.minZ, (double) baseBB.maxX, (double) baseBB.maxY, (double) baseBB.maxZ, (float) 0.0f, (float) 0.0f, (float) 0.0f, (float) 0.4f);
					GL11.glPopMatrix();
					GlStateManager.enableTexture2D();
				}
				EnumFacing face = (EnumFacing) te.getWorld().getBlockState(te.getPos()).getValue(EnumRotation.EFACING);
				if(te.getWorld().getBlockState(te.getPos().offset(face.getOpposite())).getBlock() == BlocksPS.LASER_FIELD)
				{
					SimpleBlockRendering s = RenderBlocks.forMod(Info.MOD_ID).simpleRenderer;
					s.rb.renderAlpha = 1.0f;
					GlStateManager.color((float) 1.0f, (float) 1.0f, (float) 1.0f, (float) 1.0f);
					GlStateManager.enableNormalize();
					GlStateManager.enableBlend();
					GlStateManager.blendFunc((GlStateManager.SourceFactor) GlStateManager.SourceFactor.SRC_ALPHA, (GlStateManager.DestFactor) GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
					s.begin();
					s.setBrightness(this.getBrightnessForRB(te, s.rb));
					s.setSprite(Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(Info.MOD_ID + ":blocks/laser_field"));
					s.setRenderBounds(BlockFizzlerPart.EnumFizzlerOrientation.valueOf(face.getAxis().name()).getBoundary());
					s.drawBlock(x, y, z);
					s.end();
				}
			}
		} catch(Throwable fizz)
		{
			// empty catch block
		}
	}
}