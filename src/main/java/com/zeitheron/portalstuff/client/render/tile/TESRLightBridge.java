package com.zeitheron.portalstuff.client.render.tile;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.portalstuff.client.shader.RenderLightBridgePart;
import com.zeitheron.portalstuff.init.BlocksPS;
import com.zeitheron.portalstuff.tile.TileLightBridge;

import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;

public class TESRLightBridge extends TESR<TileLightBridge>
{
	private static final ItemStack stack = new ItemStack(BlocksPS.LIGHT_BRIDGE_PART);
	
	@Override
	public void renderTileEntityAt(TileLightBridge te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		if(te.face == null)
			return;
		GL11.glPushMatrix();
		GL11.glTranslated((double) x, (double) (y + 0.375 + (double) te.height), (double) z);
		if(te.face == EnumFacing.WEST)
		{
			GL11.glTranslated((double) 1.0, (double) 0.0, (double) 0.0);
		}
		if(te.face == EnumFacing.NORTH)
		{
			GL11.glTranslated((double) 1.0, (double) 0.0, (double) 1.0);
		}
		if(te.face == EnumFacing.EAST)
		{
			GL11.glTranslated((double) 0.0, (double) 0.0, (double) 1.0);
		}
		GL11.glRotated((double) (te.face == EnumFacing.SOUTH ? 0.0 : (te.face == EnumFacing.EAST ? 90.0 : (te.face == EnumFacing.WEST ? 270.0 : 180.0))), (double) 0.0, (double) 1.0, (double) 0.0);
		GL11.glTranslated((double) 0.5, (double) 0.0, (double) 0.5);
		GL11.glScaled((double) 2.0, (double) 2.0, (double) 2.0);
		this.mc.getRenderItem().renderItem(stack, ItemCameraTransforms.TransformType.FIXED);
		GL11.glPopMatrix();
		if(te.isElementActive())
			RenderLightBridgePart.addRenderPos(new AxisAlignedBB(0.0, (double) te.height, 0.0, 1.0, (double) te.height, 1.0), x, y, z);
	}
}