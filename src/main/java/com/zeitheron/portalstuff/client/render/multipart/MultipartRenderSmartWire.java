package com.zeitheron.portalstuff.client.render.multipart;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.api.multipart.IMultipartRender;
import com.zeitheron.hammercore.client.render.vertex.SimpleBlockRendering;
import com.zeitheron.hammercore.client.utils.RenderBlocks;
import com.zeitheron.portalstuff.Info;
import com.zeitheron.portalstuff.multipart.MultipartSmartWire;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

public class MultipartRenderSmartWire implements IMultipartRender<MultipartSmartWire>
{
	@Override
	public void renderMultipartAt(MultipartSmartWire part, double x, double y, double z, float prog, ResourceLocation tex)
	{
		SimpleBlockRendering s = RenderBlocks.forMod(Info.MOD_ID).simpleRenderer;
		s.begin();
		s.rb.renderAlpha = 1f;
		GlStateManager.color(1f, 1f, 1f, 1f);
		GlStateManager.enableNormalize();
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
		GL11.glPushMatrix();
		GL11.glTranslated(x, y, z);
		s.disableFaces();
		s.setBrightness(s.rb.setLighting(part.getOwner().getWorld(), part.getOwner().getPos()));
		s.enableFace(part.placedFace);
		s.setSprite(Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(Info.MOD_ID + ":blocks/smart_wire"));
		s.setColor(part.placedFace, part.isElementActive() ? 16757570 : 4369919);
		s.setRenderBounds(part.getBoundingBox());
		s.drawBlock(0.0, 0.0, 0.0);
		s.end();
		GL11.glPopMatrix();
	}
}