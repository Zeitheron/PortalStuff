package com.zeitheron.portalstuff.client.render.tile;

import java.util.List;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.utils.VecDir;
import com.zeitheron.hammercore.utils.math.vec.Vector3;
import com.zeitheron.portalstuff.tile.TileLaserEmitter;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.Vec3d;

public class TESRLaserEmitter extends TESR<TileLaserEmitter>
{
	@Override
	public void renderTileEntityAt(TileLaserEmitter te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		if(!te.isActive)
			return;
		List<VecDir> dirs = te.info.dirs;
		Vec3d off = new Vec3d(te.getPos()).add(0.5, 0.5, 0.5);
		if(dirs != null)
		{
			float width = GL11.glGetFloat((int) 2849);
			GL11.glPushMatrix();
			GL11.glTranslated((double) (x + 0.5), (double) (y + 0.5), (double) (z + 0.5));
			GlStateManager.disableLighting();
			RenderHelper.disableStandardItemLighting();
			for(int i = 0; i < dirs.size(); ++i)
			{
				VecDir dir = dirs.get(i);
				Vec3d start = dir.start.subtract(off);
				Vec3d end = dir.calcEndVec().subtract(off);
				RenderUtil.drawLine((Vector3) new Vector3(start), (Vector3) new Vector3(end), (int) -14540033, (float) ((float) (Math.cos((float) te.ticksExisted / 3.0f) + 3.0) * 2.0f));
			}
			GL11.glPopMatrix();
			GL11.glLineWidth((float) width);
		}
	}
}