package com.zeitheron.portalstuff.client.shader;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.render.shader.HCShaderPipeline;
import com.zeitheron.hammercore.client.render.shader.IShaderOperation;
import com.zeitheron.hammercore.client.render.shader.ShaderProgram;
import com.zeitheron.hammercore.client.utils.RenderBlocks;
import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.portalstuff.Info;
import com.zeitheron.portalstuff.blocks.BlockFizzlerPart;
import com.zeitheron.portalstuff.cfg.ConfigsPS;
import com.zeitheron.portalstuff.client.FizzlerRedificator;
import com.zeitheron.portalstuff.client.render.virtual.portalplacers.PortalPlacerRender;
import com.zeitheron.portalstuff.init.BlocksPS;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class RenderFizzler
{
	public static FizzlerShaderOperation shader;
	private static final List<BlockPos> renderedPositions;
	
	public static boolean experimentalShader = true;
	
	public static void initExperimental(boolean e)
	{
		experimentalShader = e;
		initFizzlerShader();
	}
	
	public static void addRenderPos(BlockPos pos)
	{
		renderedPositions.add(pos);
	}
	
	public static void initFizzlerShader()
	{
		try
		{
			if(shader != null)
				RenderFizzler.shader.program.cleanup();
			int op = HCShaderPipeline.registerOperation();
			ShaderProgram prog = new ShaderProgram();
			shader = new FizzlerShaderOperation(prog, op);
			
			if(experimentalShader)
				prog.attachFrag("/assets/" + Info.MOD_ID + "/shaders/noise.fsh").attachVert("/assets/" + Info.MOD_ID + "/shaders/noise.vsh").attachShaderOperation(shader);
			else
				prog.attachFrag("/assets/" + Info.MOD_ID + "/shaders/fizzlers.fsh").attachVert("/assets/" + Info.MOD_ID + "/shaders/fizzlers.vsh").attachShaderOperation(shader);
			
			prog.validate();
		} catch(Throwable err)
		{
			shader = null;
			err.printStackTrace();
		}
	}
	
	@SubscribeEvent
	public void renderLast(RenderWorldLastEvent evt)
	{
		if(shader == null && useShaders())
			RenderFizzler.initFizzlerShader();
		
//		initExperimental(true);
		
		PortalPlacerRender.render();
		RenderLightBridgePart.render();
		try
		{
			if(renderedPositions.isEmpty())
				return;
			
			if(useShaders())
				shader.program.freeBindShader();
			
			Minecraft.getMinecraft().entityRenderer.disableLightmap();
			RenderHelper.disableStandardItemLighting();
			
			GL11.glEnable(3042);
			Tessellator.getInstance().getBuffer().begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_LMAP_COLOR);
			RenderBlocks rb = RenderBlocks.getInstance();
			TextureAtlasSprite sprite = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(Info.MOD_ID + ":blocks/fizzler_part");
			int index = 0;
			while(!renderedPositions.isEmpty())
			{
				++index;
				BlockPos pos = renderedPositions.remove(0);
				double x = pos.getX() - TileEntityRendererDispatcher.staticPlayerX;
				double y = pos.getY() - TileEntityRendererDispatcher.staticPlayerY;
				double z = pos.getZ() - TileEntityRendererDispatcher.staticPlayerZ;
				GL11.glPushMatrix();
				try
				{
					double xx;
					double zz;
					WorldClient world = Minecraft.getMinecraft().world;
					IBlockState state = world.getBlockState(pos);
					if(state.getBlock() == BlocksPS.FIZZLER_PART)
					{
						BlockFizzlerPart.EnumFizzlerOrientation ori = (BlockFizzlerPart.EnumFizzlerOrientation) ((Object) state.getValue(BlockFizzlerPart.EnumFizzlerOrientation.ORIENTATION));
						xx = ori == BlockFizzlerPart.EnumFizzlerOrientation.Z ? 0.5 : 0.0;
						zz = ori == BlockFizzlerPart.EnumFizzlerOrientation.X ? 0.5 : 0.0;
						rb.setRenderBounds(x + 0.5 - xx, y, z + 0.5 - zz, x + 0.5 + xx, y + 1.0, z + 0.5 + zz);
					}
					if(state.getBlock() == BlocksPS.FIZZLER)
					{
						Axis ori = state.getValue(EnumRotation.EFACING).getAxis();
						xx = ori == EnumFacing.Axis.X ? 0.5 : 0.0;
						zz = ori == EnumFacing.Axis.Z ? 0.5 : 0.0;
						rb.setRenderBounds(x + 0.5 - xx, y, z + 0.5 - zz, x + 0.5 + xx, y + 1.0, z + 0.5 + zz);
					}
					
					float redificator = 0.4f;
					float redify = FizzlerRedificator.getRedificationProgress() * redificator;
					float r = 1.0f - redificator + redify;
					float g = 1.0f - redificator + redify;
					float b = 1.0f - redificator + redify;
					
					rb.renderFaceXNeg(0.0, 0.0, 0.0, sprite, r, g, b, 255);
					rb.renderFaceXPos(0.0, 0.0, 0.0, sprite, r, g, b, 255);
					rb.renderFaceZNeg(0.0, 0.0, 0.0, sprite, r, g, b, 255);
					rb.renderFaceZPos(0.0, 0.0, 0.0, sprite, r, g, b, 255);
				} catch(Throwable err)
				{
					err.printStackTrace();
				}
				GL11.glPopMatrix();
			}
			Tessellator.getInstance().draw();
			if(RenderFizzler.useShaders())
				ShaderProgram.unbindShader();
		} catch(Throwable err)
		{
			err.printStackTrace();
		}
	}
	
	public static boolean useShaders()
	{
		return OpenGlHelper.shadersSupported && ConfigsPS.client_renderShader;
	}
	
	static
	{
		renderedPositions = new ArrayList<BlockPos>();
		RenderFizzler.initFizzlerShader();
	}
	
	public static class FizzlerShaderOperation implements IShaderOperation
	{
		public final ShaderProgram program;
		public final int op;
		
		public FizzlerShaderOperation(ShaderProgram prog, int op)
		{
			this.program = prog;
			this.op = op;
		}
		
		@Override
		public boolean load(ShaderProgram program)
		{
			return true;
		}
		
		@Override
		public void operate(ShaderProgram program)
		{
			if(experimentalShader)
			{
				ARBShaderObjects.glUniform2iARB(program.getUniformLoc("uResolution"), 256, 256);
				ARBShaderObjects.glUniform1fARB(program.getUniformLoc("uTime"), (float) ((double) (System.currentTimeMillis() % 10000000L) / 1000.0));
				ARBShaderObjects.glUniform2iARB(program.getUniformLoc("uSeed"), 0, 0);
				ARBShaderObjects.glUniform1fARB(program.getUniformLoc("uNoiseScale"), 1F);
				
				float redificator = 0.6f;
				float redify = FizzlerRedificator.getRedificationProgress() * redificator;
				float r = 0;
				float g = .75F;
				float b = 1F + redify / 2F;
				float a = 0.2F + FizzlerRedificator.getRedificationProgress() * 0.1F;
				
				ARBShaderObjects.glUniform4fARB(program.getUniformLoc("uColor"), r, g, b, a);
			} else
			{
				ARBShaderObjects.glUniform1fARB(program.getUniformLoc("time"), (float) ((double) (System.currentTimeMillis() % 10000000L) / 1000.0));
				ARBShaderObjects.glUniform1fARB(program.getUniformLoc("seed"), 0F);
				try
				{
					ARBShaderObjects.glUniform1fARB((int) program.getUniformLoc("redify"), (float) (0.2f + FizzlerRedificator.getRedificationProgress() * 0.1f));
				} catch(Throwable throwable)
				{
					// empty catch block
				}
			}
		}
		
		@Override
		public int operationID()
		{
			return this.op;
		}
	}
}