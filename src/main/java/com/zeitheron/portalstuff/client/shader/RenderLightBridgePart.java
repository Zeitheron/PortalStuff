package com.zeitheron.portalstuff.client.shader;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.render.shader.HCShaderPipeline;
import com.zeitheron.hammercore.client.render.shader.IShaderOperation;
import com.zeitheron.hammercore.client.render.shader.ShaderProgram;
import com.zeitheron.hammercore.client.render.vertex.SimpleBlockRendering;
import com.zeitheron.hammercore.client.utils.RenderBlocks;
import com.zeitheron.portalstuff.Info;
import com.zeitheron.portalstuff.tile.TileLightBridge;
import com.zeitheron.portalstuff.tile.TileLightBridgePart;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;

public class RenderLightBridgePart
{
	public static LightBridgeShaderOperation shader;
	private static final List<AxisAlignedBB> renders;
	private static final List<Double> renderXs;
	private static final List<Double> renderYs;
	private static final List<Double> renderZs;
	
	public static void addRenderPos(AxisAlignedBB aabb, double x, double y, double z)
	{
		renders.add(aabb);
		renderXs.add(x);
		renderYs.add(y);
		renderZs.add(z);
	}
	
	public static void render()
	{
		block27:
		{
			if(shader == null)
				RenderLightBridgePart.initLightBridgeShader();
			try
			{
				if(renders.isEmpty())
					break block27;
				GL11.glEnable(GL11.GL_BLEND);
				for(int i = 0; i < renders.size(); ++i)
				{
					double x = renderXs.get(i);
					double y = renderYs.get(i);
					double z = renderZs.get(i);
					BlockPos pos = new BlockPos(x + TileEntityRendererDispatcher.staticPlayerX, y + TileEntityRendererDispatcher.staticPlayerY, z + TileEntityRendererDispatcher.staticPlayerZ);
					TileEntity tile = Minecraft.getMinecraft().world.getTileEntity(pos);
					GL11.glPushMatrix();
					EnumFacing orientate = EnumFacing.NORTH;
					if(tile instanceof TileLightBridgePart)
					{
						try
						{
							TileLightBridgePart te2 = (TileLightBridgePart) tile;
							if(te2.towards != null)
								orientate = te2.towards;
						} catch(Throwable te2)
						{
							// empty catch block
						}
					}
					if(tile instanceof TileLightBridge)
					{
						try
						{
							TileLightBridge te2 = (TileLightBridge) tile;
							if(te2.face != null)
								orientate = te2.face;
						} catch(Throwable te3)
						{
							// empty catch block
						}
					}
					GL11.glTranslated(x, y, z);
					if(orientate.getAxis() == EnumFacing.Axis.Z)
					{
						GL11.glTranslated((double) 0.0, (double) 0.0, (double) 1.0);
						GL11.glRotated((double) 90.0, (double) 0.0, (double) 1.0, (double) 0.0);
					}
					SimpleBlockRendering sbr = RenderBlocks.forMod(Info.MOD_ID).simpleRenderer;
					sbr.begin();
					if(tile instanceof TileLightBridgePart)
						sbr.setRenderBounds(((TileLightBridgePart) tile).aabb);
					if(tile instanceof TileLightBridge)
						sbr.setRenderBounds(0.0, (double) ((TileLightBridge) tile).height, 0.0, 1.0, (double) ((TileLightBridge) tile).height, 1.0);
					sbr.setSprite(Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(Info.MOD_ID + ":blocks/light_bridge" + (RenderFizzler.useShaders() ? "_background" : "")));
					sbr.drawBlock(0.0, 0.0, 0.0);
					sbr.end();
					GL11.glPopMatrix();
				}
				
				if(!RenderFizzler.useShaders())
				{
					renders.clear();
					renderXs.clear();
					renderYs.clear();
					renderZs.clear();
					return;
				}
				
				if(RenderFizzler.useShaders())
					shader.program.freeBindShader();
				
				RenderBlocks rb = RenderBlocks.forMod(Info.MOD_ID);
				TextureAtlasSprite sprite = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(Info.MOD_ID + ":blocks/light_bridge");
				
				while(!renders.isEmpty())
				{
					Tessellator.getInstance().getBuffer().begin(7, DefaultVertexFormats.POSITION_TEX_LMAP_COLOR);
					AxisAlignedBB aabb = renders.remove(0).grow(0.004).offset(0.0, -0.002, 0.0);
					double x = renderXs.remove(0);
					double y = renderYs.remove(0);
					double z = renderZs.remove(0);
					BlockPos pos = new BlockPos(x + TileEntityRendererDispatcher.staticPlayerX, y + TileEntityRendererDispatcher.staticPlayerY, z + TileEntityRendererDispatcher.staticPlayerZ);
					GL11.glPushMatrix();
					EnumFacing orientate = EnumFacing.NORTH;
					try
					{
						TileEntity tile = Minecraft.getMinecraft().world.getTileEntity(pos);
						if(tile instanceof TileLightBridgePart)
						{
							TileLightBridgePart part = (TileLightBridgePart) tile;
							if(part.towards != null)
							{
								orientate = part.towards;
							}
						} else if(tile instanceof TileLightBridge)
						{
							TileLightBridge br = (TileLightBridge) tile;
							orientate = br.face;
						}
					} catch(Throwable tile)
					{
						// empty catch block
					}
					GL11.glTranslated((double) x, (double) (y + 0.005), (double) z);
					if(orientate.getAxis() == EnumFacing.Axis.Z)
					{
						GL11.glTranslated((double) 0.0, (double) 0.0, (double) 1.0);
						GL11.glRotated((double) 90.0, (double) 0.0, (double) 1.0, (double) 0.0);
					}
					if(orientate == EnumFacing.NORTH || orientate == EnumFacing.EAST)
					{
						GL11.glTranslated((double) -0.5, (double) -0.5, (double) -0.5);
						GL11.glRotated((double) 180.0, (double) 0.0, (double) 1.0, (double) 0.0);
						GL11.glTranslated((double) -1.5, (double) 0.5, (double) -1.5);
					}
					EnumFacing face = orientate.getAxis() != EnumFacing.Axis.Y ? EnumFacing.UP : (orientate.getAxis() != EnumFacing.Axis.X ? EnumFacing.EAST : EnumFacing.SOUTH);
					try
					{
						rb.setRenderBounds(aabb.minX, aabb.minY, aabb.minZ, aabb.maxX, aabb.maxY, aabb.maxZ);
						rb.renderFace(face, 0.0, 0.0, 0.0, sprite, 1.0f, 1.0f, 1.0f, 255);
						rb.renderFace(face.getOpposite(), 0.0, 0.0, 0.0, sprite, 1.0f, 1.0f, 1.0f, 255);
					} catch(Throwable err)
					{
						err.printStackTrace();
					}
					Tessellator.getInstance().draw();
					GL11.glPopMatrix();
				}
				ShaderProgram.unbindShader();
			} catch(Throwable err)
			{
				err.printStackTrace();
			}
		}
	}
	
	public static void initLightBridgeShader()
	{
		try
		{
			if(shader != null)
				RenderLightBridgePart.shader.program.cleanup();
			int op = HCShaderPipeline.registerOperation();
			ShaderProgram prog = new ShaderProgram();
			shader = new LightBridgeShaderOperation(prog, op);
			prog.attachFrag("/assets/" + Info.MOD_ID + "/shaders/light_bridge.fsh").attachVert("/assets/" + Info.MOD_ID + "/shaders/light_bridge.vsh").attachShaderOperation(shader);
			prog.validate();
		} catch(Throwable err)
		{
			shader = null;
			err.printStackTrace();
		}
	}
	
	static
	{
		renders = new ArrayList<AxisAlignedBB>();
		renderXs = new ArrayList<Double>();
		renderYs = new ArrayList<Double>();
		renderZs = new ArrayList<Double>();
		RenderLightBridgePart.initLightBridgeShader();
	}
	
	public static class LightBridgeShaderOperation implements IShaderOperation
	{
		public final ShaderProgram program;
		public final int op;
		
		public LightBridgeShaderOperation(ShaderProgram prog, int op)
		{
			this.program = prog;
			this.op = op;
		}
		
		@Override
		public boolean load(ShaderProgram program)
		{
			return true;
		}
		
		@Override
		public void operate(ShaderProgram program)
		{
			ARBShaderObjects.glUniform1fARB(program.getUniformLoc("time"), (float) (Minecraft.getMinecraft().world.getTotalWorldTime() / 20D) + Minecraft.getMinecraft().getRenderPartialTicks() / 20F);
			ARBShaderObjects.glUniform2fARB(program.getUniformLoc("resolution"), .3725F, .745F * 1.2F);
			ARBShaderObjects.glUniform1fARB(program.getUniformLoc("offset"), -.2F);
		}
		
		@Override
		public int operationID()
		{
			return this.op;
		}
	}
}