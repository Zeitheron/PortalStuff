package com.zeitheron.portalstuff.client.sound;

import org.lwjgl.openal.AL10;
import org.lwjgl.openal.AL11;

import com.zeitheron.portalstuff.Info;

public abstract class SoundFilterEffect
{
	protected boolean isLoaded = false;
	protected boolean isEnabled = false;
	protected int id = -1;
	protected int slot = -1;
	
	public abstract void loadFilter();
	
	public abstract void checkParameters();
	
	public abstract void loadParameters();
	
	private static int safeID(SoundFilterEffect filter)
	{
		return filter != null && filter.isEnabled && filter.id != -1 ? filter.id : 0;
	}
	
	private static int safeSlot(SoundFilterEffect filter)
	{
		return filter != null && filter.isEnabled && filter.slot != -1 ? filter.slot : 0;
	}
	
	public static void load3SourceFilters(int sourceChannel, int type, SoundFilterEffect filter1, SoundFilterEffect filter2, SoundFilterEffect filter3) throws RuntimeException
	{
		AL11.alSource3i((int) sourceChannel, (int) type, (int) SoundFilterEffect.safeSlot(filter1), (int) SoundFilterEffect.safeSlot(filter2), (int) SoundFilterEffect.safeSlot(filter3));
		if(SoundFilterEffect.checkError("load3SourceFilters attempt 1") != 0)
		{
			if(filter1 != null)
			{
				filter1.isLoaded = false;
				filter1.loadFilter();
			}
			if(filter2 != null)
			{
				filter2.isLoaded = false;
				filter2.loadFilter();
			}
			if(filter3 != null)
			{
				filter3.isLoaded = false;
				filter3.loadFilter();
			}
			if(SoundFilterEffect.checkError("load3SourceFilters attempt 2") != 0)
				throw new RuntimeException(Info.MOD_NAME + " - Error while trying to load 3 source filters.");
		}
	}
	
	public static void loadSourceFilter(int sourceChannel, int type, SoundFilterEffect filter) throws RuntimeException
	{
		AL10.alSourcei((int) sourceChannel, (int) type, (int) SoundFilterEffect.safeSlot(filter));
		if(SoundFilterEffect.checkError("loadSourceFilter attempt 1") != 0)
		{
			if(filter != null)
			{
				filter.isLoaded = false;
				filter.loadFilter();
			}
			AL10.alSourcei((int) sourceChannel, (int) type, (int) SoundFilterEffect.safeSlot(filter));
			if(SoundFilterEffect.checkError("loadSourceFilter attempt 2") != 0)
				throw new RuntimeException(Info.MOD_NAME + " - Error while trying to load source filter.");
		}
	}
	
	public boolean isLoaded()
	{
		return this.isLoaded;
	}
	
	public boolean isEnabled()
	{
		return this.isEnabled;
	}
	
	public void enable()
	{
		this.isEnabled = true;
	}
	
	public void disable()
	{
		this.isEnabled = false;
	}
	
	private static int checkError(String location)
	{
		int err = AL10.alGetError();
		if(err != 0)
		{
			System.out.println("[" + Info.MOD_NAME + "] Caught AL error in '" + location + "'! Error is " + err);
			return err;
		}
		return 0;
	}
}
