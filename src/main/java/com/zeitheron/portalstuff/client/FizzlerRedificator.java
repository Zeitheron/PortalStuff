package com.zeitheron.portalstuff.client;

public class FizzlerRedificator
{
	public static long redifyTimer;
	public static long redifyTime;
	
	public static void redify(long forMillis)
	{
		redifyTimer = System.currentTimeMillis();
		redifyTime = forMillis;
	}
	
	public static float getRedificationProgress()
	{
		long past = System.currentTimeMillis() - redifyTimer;
		if(past > 0 && past < redifyTime)
		{
			float progress = (float) past / (float) redifyTime * 2.0f;
			if(progress > 1.0f)
			{
				progress = 2.0f - progress;
			}
			return progress;
		}
		return 0.0f;
	}
}