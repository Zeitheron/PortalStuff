package com.zeitheron.portalstuff.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.hammercore.utils.math.MathHelper;
import com.zeitheron.portalstuff.api.cube.ICubeDestructionListener;
import com.zeitheron.portalstuff.api.entity.ICube;

import me.ichun.mods.ichunutil.common.grab.GrabHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializer;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;

public abstract class EntityCubeBase extends EntityLivingBase implements ICube
{
	private static final DataParameter<Float> YAW = EntityDataManager.createKey(EntityCubeBase.class, (DataSerializer) DataSerializers.FLOAT);
	public final List<ICubeDestructionListener> deathListeners = new ArrayList<ICubeDestructionListener>();
	
	public EntityCubeBase(World world)
	{
		super(world);
		this.setSize(1.0f, 1.0f);
	}
	
	public void addDeathListener(ICubeDestructionListener l)
	{
		this.deathListeners.add(l);
	}
	
	@Override
	protected void entityInit()
	{
		super.entityInit();
		this.getDataManager().register(YAW, Float.valueOf(0.0f));
	}
	
	@Override
	public void onUpdate()
	{
		super.onUpdate();
		
		// setDead();
		
		if(!this.world.isRemote)
		{
			List<EntityPlayer> players = this.world.getPlayers(EntityPlayer.class, player -> !GrabHandler.getHandlers((EntityLivingBase) player, (Side) Side.SERVER).isEmpty());
			for(EntityPlayer player2 : players)
			{
				ArrayList<GrabHandler> handlers = GrabHandler.getHandlers((EntityLivingBase) player2, (Side) Side.SERVER);
				for(GrabHandler h : handlers)
				{
					if(h.grabbed != this)
						continue;
					this.rotationYaw = player2.rotationYaw;
				}
			}
		}
		
		if(!this.world.isRemote)
			this.getDataManager().set(YAW, rotationYaw);
		else
			this.rotationYaw = getDataManager().get(YAW);
	}
	
	@Override
	public boolean canBeCollidedWith()
	{
		return !this.isDead;
	}
	
	@Override
	public AxisAlignedBB getCollisionBoundingBox()
	{
		return null;
	}
	
	@Override
	public AxisAlignedBB getCollisionBox(Entity entityIn)
	{
		return null;
	}
	
	@Override
	public boolean attackEntityFrom(DamageSource source, float amount)
	{
		if(this.isEntityInvulnerable(source))
			return false;
		this.setDead();
		return true;
	}
	
	@Override
	public void setDead()
	{
		if(!world.isRemote)
			for(int i = 0; i < 128; ++i)
			{
				double x = getEntityBoundingBox().minX + world.rand.nextDouble() * width;
				double y = getEntityBoundingBox().minY + world.rand.nextDouble() * height;
				double z = getEntityBoundingBox().minZ + world.rand.nextDouble() * width;
				double cx = getEntityBoundingBox().minX + width / 2;
				double cy = getEntityBoundingBox().minY + height / 2;
				double cz = getEntityBoundingBox().minZ + width / 2;
				double d = .02;
				double mx = MathHelper.clip(x - cx, -d, d);
				double my = MathHelper.clip(y - cy, -d, d);
				double mz = MathHelper.clip(z - cz, -d, d);
				if(world.rand.nextBoolean())
					HCNet.spawnParticle(world, EnumParticleTypes.CLOUD, x, y, z, mx, my, mz);
				if(world.rand.nextBoolean())
					HCNet.spawnParticle(world, EnumParticleTypes.END_ROD, x, y, z, mx * 1.8, my * 1.8, mz * 1.8);
				if(!world.rand.nextBoolean())
					continue;
				HCNet.spawnParticle(world, EnumParticleTypes.SMOKE_NORMAL, x, y, z, mx * 1.8, my * 1.8, mz * 1.8);
			}
		
		super.setDead();
		this.setPositionAndUpdate(this.posX, -1000.0, this.posZ);
		for(ICubeDestructionListener l : this.deathListeners)
			l.onDestructed(this);
	}
	
	@Override
	public boolean canBeAttackedWithItem()
	{
		return true;
	}
	
	@Override
	public boolean canBePushed()
	{
		return true;
	}
	
	@Override
	public boolean isEntityInvulnerable(DamageSource source)
	{
		return source == DamageSource.FALL || source == DamageSource.IN_WALL || source.isFireDamage() || source.isMagicDamage();
	}
	
	@Override
	public Iterable<ItemStack> getArmorInventoryList()
	{
		return Collections.emptyList();
	}
	
	@Override
	public ItemStack getItemStackFromSlot(EntityEquipmentSlot slotIn)
	{
		return InterItemStack.NULL_STACK;
	}
	
	@Override
	public void setItemStackToSlot(EntityEquipmentSlot slotIn, ItemStack stack)
	{
	}
	
	@Override
	public EnumHandSide getPrimaryHand()
	{
		return EnumHandSide.LEFT;
	}
}