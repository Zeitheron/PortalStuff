package com.zeitheron.portalstuff.entity;

import com.zeitheron.portalstuff.api.entity.ICube;

import net.minecraft.world.World;

public class EntityCubeCompanion extends EntityCubeBase
{
	public EntityCubeCompanion(World worldIn)
	{
		super(worldIn);
	}
	
	@Override
	public ICube.EnumCubeType getCubeType()
	{
		return ICube.EnumCubeType.COMPANION;
	}
}