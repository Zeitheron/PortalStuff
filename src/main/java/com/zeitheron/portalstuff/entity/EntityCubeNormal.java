package com.zeitheron.portalstuff.entity;

import com.zeitheron.portalstuff.api.entity.ICube;

import net.minecraft.world.World;

public class EntityCubeNormal extends EntityCubeBase
{
	public EntityCubeNormal(World worldIn)
	{
		super(worldIn);
	}
	
	@Override
	public ICube.EnumCubeType getCubeType()
	{
		return ICube.EnumCubeType.NORMAL;
	}
}