package com.zeitheron.portalstuff.entity;

import com.zeitheron.portalstuff.api.entity.ICube;
import com.zeitheron.portalstuff.api.entity.ILaserTriggerable;

import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializer;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.world.World;

public class EntityCubeReflection extends EntityCubeBase implements ILaserTriggerable
{
	private static final DataParameter<Boolean> LASERED = EntityDataManager.createKey(EntityCubeReflection.class, (DataSerializer) DataSerializers.BOOLEAN);
	
	public EntityCubeReflection(World worldIn)
	{
		super(worldIn);
		this.setSize(1.0f, 1.0f);
	}
	
	@Override
	protected void entityInit()
	{
		super.entityInit();
		this.getDataManager().register(LASERED, false);
	}
	
	@Override
	public void onUpdate()
	{
		super.onUpdate();
	}
	
	@Override
	public void toggle(boolean lasered)
	{
		getDataManager().set(LASERED, lasered);
	}
	
	public boolean isLasered()
	{
		return getDataManager().get(LASERED);
	}
	
	@Override
	public ICube.EnumCubeType getCubeType()
	{
		return ICube.EnumCubeType.REFLECTION;
	}
}
