package com.zeitheron.portalstuff.items;

import com.zeitheron.hammercore.api.multipart.ItemBlockMultipartProvider;
import com.zeitheron.portalstuff.multipart.MultipartSmartWire;

public class ItemSmartWire extends ItemBlockMultipartProvider
{
	public ItemSmartWire()
	{
		super((index, stack, player, world, pos, face, hx, hy, hz) -> new MultipartSmartWire(face));
		this.setTranslationKey("smart_wire");
	}
}