package com.zeitheron.portalstuff.items;

import com.zeitheron.portalstuff.entity.EntityCubeNormal;

import net.minecraft.world.World;

public class ItemCubeNormal extends ItemCubeBase<EntityCubeNormal>
{
	public ItemCubeNormal()
	{
		super("weighted");
	}
	
	@Override
	public EntityCubeNormal newCube(World world)
	{
		return new EntityCubeNormal(world);
	}
}