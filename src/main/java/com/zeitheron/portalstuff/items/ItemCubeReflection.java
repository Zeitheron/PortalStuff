package com.zeitheron.portalstuff.items;

import com.zeitheron.portalstuff.entity.EntityCubeReflection;

import net.minecraft.world.World;

public class ItemCubeReflection extends ItemCubeBase<EntityCubeReflection>
{
	public ItemCubeReflection()
	{
		super("reflection");
	}
	
	@Override
	public EntityCubeReflection newCube(World world)
	{
		return new EntityCubeReflection(world);
	}
}