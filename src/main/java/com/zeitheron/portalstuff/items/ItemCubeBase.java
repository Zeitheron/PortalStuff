package com.zeitheron.portalstuff.items;

import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.portalstuff.entity.EntityCubeBase;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public abstract class ItemCubeBase<T extends EntityCubeBase> extends Item
{
	public ItemCubeBase(String type)
	{
		this.setTranslationKey(type + "_cube");
		this.setMaxStackSize(1);
	}

	@Override
	public EnumActionResult onItemUse(EntityPlayer playerIn, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		ItemStack stack = playerIn.getHeldItem(hand);
		IBlockState iblockstate = worldIn.getBlockState(pos);
		Block block = iblockstate.getBlock();
		if(!block.isReplaceable(worldIn, pos))
			pos = pos.offset(facing);
		if(!InterItemStack.isStackNull(stack) && playerIn.canPlayerEdit(pos, facing, stack) && worldIn.mayPlace(Blocks.STONE, pos, false, facing, playerIn))
		{
			int i = this.getMetadata(stack.getMetadata());
			if(placeBlockAt(stack, playerIn, worldIn, pos, facing, hitX, hitY, hitZ))
			{
				SoundType soundtype = SoundType.METAL;
				worldIn.playSound(playerIn, pos, soundtype.getPlaceSound(), SoundCategory.BLOCKS, (soundtype.getVolume() + 1.0f) / 2.0f, soundtype.getPitch() * 0.8f);
				stack.shrink(1);
			}
			return EnumActionResult.SUCCESS;
		}
		return EnumActionResult.FAIL;
	}
	
	public boolean placeBlockAt(ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if(world.isRemote)
			return true;
		T qube = newCube(world);
		qube.setPositionAndUpdate(pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5);
		qube.rotationYaw = qube.prevRotationYaw = player.rotationYaw;
		qube.rotationPitch = qube.prevRotationPitch = player.rotationPitch;
		return world.spawnEntity(qube);
	}
	
	public abstract T newCube(World world);
}