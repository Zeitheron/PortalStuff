package com.zeitheron.portalstuff.items;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zeitheron.hammercore.lib.zlib.json.JSONObject;
import com.zeitheron.hammercore.lib.zlib.json.JSONTokener;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.portalstuff.virtual.portalplacers.PortalPlacer;
import com.zeitheron.portalstuff.virtual.portalplacers.PortalPlacerListManager;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.Tuple;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemPortalPlacer extends Item
{
	private Map<String, Tuple<Integer, Integer>> widths = new HashMap<String, Tuple<Integer, Integer>>();
	
	public ItemPortalPlacer()
	{
		this.setTranslationKey("portal_placer");
	}

	@Override
	public EnumActionResult onItemUse(EntityPlayer playerIn, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		if(worldIn.isRemote)
			return EnumActionResult.FAIL;
		if(facing.getAxis() == EnumFacing.Axis.Y)
		{
			PortalPlacer placer = new PortalPlacer("Global", "Chell");
			placer.positions[0] = pos.offset(facing);
			placer.positions[1] = pos.offset(facing).offset(playerIn.getHorizontalFacing());
			placer.placed = facing;
			placer.isTypeA = playerIn.isSneaking();
			placer.up = playerIn.getHorizontalFacing();
			if(PortalPlacerListManager.getPlacersListFor(worldIn).setPlacer(placer))
				HCNet.swingArm(playerIn, hand);
			if(playerIn instanceof EntityPlayerMP)
				PortalPlacerListManager.sendVisiblePlacersToPlayer((EntityPlayerMP) playerIn);
			return EnumActionResult.SUCCESS;
		}
		PortalPlacer placer = new PortalPlacer("Global", "Chell");
		placer.positions[0] = pos.offset(facing);
		placer.positions[1] = pos.offset(facing).up();
		placer.placed = facing;
		placer.isTypeA = playerIn.isSneaking();
		placer.up = null;
		if(PortalPlacerListManager.getPlacersListFor(worldIn).setPlacer(placer))
			HCNet.swingArm(playerIn, hand);
		if(playerIn instanceof EntityPlayerMP)
			PortalPlacerListManager.sendVisiblePlacersToPlayer((EntityPlayerMP) playerIn);
		return EnumActionResult.SUCCESS;
	}

	@Override
	@SideOnly(value = Side.CLIENT)
	public void addInformation(ItemStack stack, World world, List<String> tooltip, ITooltipFlag advanced)
	{
		int width = 1;
		int height = 2;
		if(stack.hasDisplayName())
		{
			String dn = stack.getDisplayName();
			if(this.widths.containsKey(dn))
			{
				width = (Integer) this.widths.get(dn).getFirst();
				height = (Integer) this.widths.get(dn).getSecond();
			} else
			{
				try
				{
					JSONObject obj = (JSONObject) new JSONTokener(dn).nextValue();
					width = obj.optInt("w", 1);
					height = obj.optInt("h", 2);
					this.widths.put(dn, (Tuple) new Tuple((Object) width, (Object) height));
				} catch(Throwable err)
				{
					stack.setStackDisplayName((Object) TextFormatting.RED + "Invalid JSON!");
					ByteArrayOutputStream n = new ByteArrayOutputStream();
					PrintStream ps = new PrintStream(n);
					err.printStackTrace(ps);
					ps.close();
					String[] str = new String(n.toByteArray()).replaceAll("\t", "  ").replaceAll("\r", "").split("\n");
					for(int i = 0; i < Math.min(str.length, 2); ++i)
					{
						tooltip.add(str[i]);
					}
				}
			}
		} else
		{
			// tooltip.add("Hint: rename to {w:2,h:2} to make portals 2x2");
		}
		tooltip.add("Width: " + width + " (NYI)");
		tooltip.add("Height: " + height + " (NYI)");
	}
}