package com.zeitheron.portalstuff.items;

import com.zeitheron.portalstuff.entity.EntityCubeCompanion;

import net.minecraft.world.World;

public class ItemCubeCompanion extends ItemCubeBase<EntityCubeCompanion>
{
	public ItemCubeCompanion()
	{
		super("companion");
	}
	
	@Override
	public EntityCubeCompanion newCube(World world)
	{
		return new EntityCubeCompanion(world);
	}
}