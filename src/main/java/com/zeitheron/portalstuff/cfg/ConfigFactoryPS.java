package com.zeitheron.portalstuff.cfg;

import java.util.Set;

import com.zeitheron.hammercore.cfg.gui.HCConfigGui;
import com.zeitheron.portalstuff.Info;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.client.IModGuiFactory;

public class ConfigFactoryPS implements IModGuiFactory
{
	@Override
	public void initialize(Minecraft minecraftInstance)
	{
	}

	@Override
	public Set<IModGuiFactory.RuntimeOptionCategoryElement> runtimeGuiCategories()
	{
		return null;
	}
	
	@Override
	public boolean hasConfigGui()
	{
		return true;
	}
	
	@Override
	public GuiScreen createConfigGui(GuiScreen parent)
	{
		return new HCConfigGui(parent, ConfigsPS.cfgs, Info.MOD_ID);
	}
}