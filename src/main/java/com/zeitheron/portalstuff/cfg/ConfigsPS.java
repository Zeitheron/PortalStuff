
package com.zeitheron.portalstuff.cfg;

import com.zeitheron.hammercore.cfg.HCModConfigurations;
import com.zeitheron.hammercore.cfg.IConfigReloadListener;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyBool;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyStringList;
import com.zeitheron.portalstuff.Info;

import net.minecraftforge.common.config.Configuration;

@HCModConfigurations(modid = Info.MOD_ID)
public class ConfigsPS implements IConfigReloadListener
{
	@ModConfigPropertyBool(category = "Client Only", comment = "Should Portal Stuff activate fancy shaders? (not gonna happen if your pc is a potato)", defaultValue = true, name = "Shaders")
	public static boolean client_renderShader;
	
	@ModConfigPropertyStringList(allowedValues = {}, name = "PortalPlacementList", category = "Placement", comment = "On what block should portals be placable?", defaultValue = { "minecraft:iron_block", "minecraft:wool[color=white]", "minecraft:snow", "minecraft:quartz_block", "minecraft:stone[variant=diorite]", "minecraft:stone[variant=smooth_diorite]", "minecraft:double_stone_slab[seamless=false,variant=quartz]" })
	public static String[] placement_placementList;
	
	@ModConfigPropertyBool(name = "PortalPlacementNeverFails", category = "Placement", comment = "If this is true then you would be able to place portal like before, on any block.", defaultValue = false)
	public static boolean placement_placementNeverFails;
	
	public static Configuration cfgs;

	@Override
	public void reloadCustom(Configuration cfgs)
	{
		ConfigsPS.cfgs = cfgs;
	}
	
	static
	{
		placement_placementList = new String[] { "minecraft:iron_block", "minecraft:wool[color=white]", "minecraft:snow", "minecraft:quartz_block", "minecraft:stone[variant=diorite]", "minecraft:stone[variant=smooth_diorite]", "minecraft:double_stone_slab[seamless=false,variant=quartz]" };
		placement_placementNeverFails = false;
	}
}
