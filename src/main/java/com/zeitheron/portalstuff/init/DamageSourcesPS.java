package com.zeitheron.portalstuff.init;

import com.zeitheron.portalstuff.Info;

import net.minecraft.util.DamageSource;

public class DamageSourcesPS
{
	public static final DamageSource LASER = new DamageSource(Info.MOD_ID + ":laser");
	public static final DamageSource LASER_FIZZLER = new DamageSource(Info.MOD_ID + ":laser_fizzler").setDamageBypassesArmor();
}