package com.zeitheron.portalstuff.init;

import com.zeitheron.portalstuff.items.ItemCubeCompanion;
import com.zeitheron.portalstuff.items.ItemCubeNormal;
import com.zeitheron.portalstuff.items.ItemCubeReflection;
import com.zeitheron.portalstuff.items.ItemHidden;
import com.zeitheron.portalstuff.items.ItemPortalPlacer;
import com.zeitheron.portalstuff.items.ItemSmartWire;

import net.minecraft.item.Item;

public class ItemsPS
{
	public static final Item REFLECTION_CUBE = new ItemCubeReflection();
	public static final Item WEIGHTED_CUBE = new ItemCubeNormal();
	public static final Item COMPANION_CUBE = new ItemCubeCompanion();
	public static final Item FLOOR_BUTTON_ANIMATED = new ItemHidden("floor_button_press");
	public static final Item SMART_WIRE = new ItemSmartWire();
	public static final Item PORTAL_PLACER = new ItemPortalPlacer();
}
