package com.zeitheron.portalstuff.init;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import com.zeitheron.portalstuff.blocks.BlockFizzler;
import com.zeitheron.portalstuff.blocks.BlockFizzlerPart;
import com.zeitheron.portalstuff.blocks.BlockFloorButtonBase;
import com.zeitheron.portalstuff.blocks.BlockLaserEmitter;
import com.zeitheron.portalstuff.blocks.BlockLaserFizzler;
import com.zeitheron.portalstuff.blocks.BlockLaserFizzlerPart;
import com.zeitheron.portalstuff.blocks.BlockLightBridge;
import com.zeitheron.portalstuff.blocks.BlockLightBridgePart;

import net.minecraft.block.Block;

public class BlocksPS
{
	public static final Block FIZZLER = new BlockFizzler();
	public static final Block FIZZLER_PART = new BlockFizzlerPart();
	public static final Block LASER_EMITTER = new BlockLaserEmitter();
	public static final Block FLOOR_BUTTON_BASE = new BlockFloorButtonBase();
	public static final Block LIGHT_BRIDGE_PART = new BlockLightBridgePart();
	public static final Block LIGHT_BRIDGE = new BlockLightBridge();
	public static final Block LASER_FIELD = new BlockLaserFizzlerPart();
	public static final Block LASER_FIZZLER = new BlockLaserFizzler();
	
	public static List<Block> getBlocks()
	{
		ArrayList<Block> blocks = new ArrayList<Block>();
		for(Field f : BlocksPS.class.getDeclaredFields())
		{
			if(!Block.class.isAssignableFrom(f.getType()))
				continue;
			try
			{
				blocks.add((Block) f.get(null));
			} catch(Throwable throwable)
			{
			}
		}
		return blocks;
	}
}
