package com.zeitheron.portalstuff.init;

import java.lang.reflect.Field;

import com.zeitheron.hammercore.internal.SimpleRegistration;
import com.zeitheron.hammercore.utils.SoundObject;
import com.zeitheron.portalstuff.Info;
import com.zeitheron.portalstuff.PortalStuff;

public class SoundsPS
{
	public static final SoundObject FIZZLER_START = new SoundObject(Info.MOD_ID, "fizzler_start");
	public static final SoundObject FIZZLER_STOP = new SoundObject(Info.MOD_ID, "fizzler_stop");
	public static final SoundObject LASER_HURT_MOB = new SoundObject(Info.MOD_ID, "laser_hurt_mob");
	public static final SoundObject FIZZLER_DISPOSE = new SoundObject(Info.MOD_ID, "fizzler_dispose");
	public static final SoundObject LASER_ACTIVATED = new SoundObject(Info.MOD_ID, "laser_activation");
	public static final SoundObject LASER_NODE_POWER_OFF = new SoundObject(Info.MOD_ID, "laser_node_power_off");
	public static final SoundObject LASER_NODE_POWER_ON = new SoundObject(Info.MOD_ID, "laser_node_power_on");
	public static final SoundObject CTX_EDITOR_OPEN = new SoundObject(Info.MOD_ID, "ctx_editor.open");
	public static final SoundObject CTX_EDITOR_CLOSE = new SoundObject(Info.MOD_ID, "ctx_editor.close");
	public static final SoundObject CTX_EDITOR_SELECT = new SoundObject(Info.MOD_ID, "ctx_editor.select");
	public static final SoundObject CTX_EDITOR_TOGGLE = new SoundObject(Info.MOD_ID, "ctx_editor.toggle");
	public static final SoundObject FOOTSTEP_LIGHT_BRIDGE = new SoundObject(Info.MOD_ID, "footsteps.lightbridge");
	public static final SoundObject DESTRUCTION_LIGHTBRIDGE_ELEC = new SoundObject(Info.MOD_ID, "destruction.lightbridge_elec");
	public static final SoundObject TBEAM_START = new SoundObject(Info.MOD_ID, "tbeam.start");
	public static final SoundObject TBEAM_TICK = new SoundObject(Info.MOD_ID, "tbeam.tick");
	public static final SoundObject TBEAM_MIDDLE = new SoundObject(Info.MOD_ID, "tbeam.middle");
	public static final SoundObject TBEAM_STOP = new SoundObject(Info.MOD_ID, "tbeam.stop");
	public static final SoundObject FLOOR_BUTTON_DOWN = new SoundObject(Info.MOD_ID, "floor_button_down");
	public static final SoundObject FLOOR_BUTTON_UP = new SoundObject(Info.MOD_ID, "floor_button_up");
	
	public static void register()
	{
		for(Field f : SoundsPS.class.getDeclaredFields())
		{
			if(!SoundObject.class.isAssignableFrom(f.getType()))
				continue;
			try
			{
				SoundObject obj = (SoundObject) f.get(null);
				SimpleRegistration.registerSound(obj);
				PortalStuff.logger.info("Registered new Sound Object (Event) (" + (Object) obj.name + ")!");
			} catch(Throwable obj)
			{
				// empty catch block
			}
		}
	}
}
