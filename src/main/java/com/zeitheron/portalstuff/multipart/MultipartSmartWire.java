package com.zeitheron.portalstuff.multipart;

import java.util.ArrayList;

import com.zeitheron.hammercore.api.handlers.IHandlerProvider;
import com.zeitheron.hammercore.api.handlers.ITileHandler;
import com.zeitheron.hammercore.api.multipart.MultipartSignature;
import com.zeitheron.hammercore.utils.WorldLocation;
import com.zeitheron.portalstuff.api.smart.SmartSwitchableManager;
import com.zeitheron.portalstuff.api.smart.ISmartSwitchable;
import com.zeitheron.portalstuff.init.ItemsPS;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;

public class MultipartSmartWire extends MultipartSignature implements ISmartSwitchable, IHandlerProvider
{
	public EnumFacing placedFace;
	public boolean isActive;
	
	public MultipartSmartWire(EnumFacing placedFace)
	{
		this.placedFace = placedFace;
	}
	
	public MultipartSmartWire()
	{
	}
	
	@Override
	public AxisAlignedBB getBoundingBox()
	{
		double s = 0.1875;
		double e = 0.8125;
		double h = 0.003125;
		double hx = 0.996875;
		
		double xn = placedFace == EnumFacing.EAST ? 0 : placedFace == EnumFacing.WEST ? hx : s;
		double xx = placedFace == EnumFacing.EAST ? h : placedFace == EnumFacing.WEST ? 1 : e;
		
		double yn = placedFace == EnumFacing.UP ? 0 : placedFace == EnumFacing.DOWN ? hx : s;
		double yx = placedFace == EnumFacing.UP ? h : placedFace == EnumFacing.DOWN ? 1 : e;
		
		double zn = placedFace == EnumFacing.SOUTH ? 0 : placedFace == EnumFacing.NORTH ? hx : s;
		double zx = placedFace == EnumFacing.SOUTH ? h : placedFace == EnumFacing.NORTH ? 1 : e;
		
		return new AxisAlignedBB(xn, yn, zn, xx, yx, zx);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setString("Plane", this.placedFace.getName().toUpperCase());
		nbt.setBoolean("Active", this.isActive);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.placedFace = EnumFacing.valueOf(nbt.getString("Plane").toUpperCase());
		this.isActive = nbt.getBoolean("Active");
	}
	
	@Override
	public void setElementActive(boolean active)
	{
		boolean changed = this.isActive;
		this.isActive = active;
		boolean bl = changed = changed != this.isActive;
		if(changed)
		{
			this.requestSync();
			ArrayList<ISmartSwitchable> switchables = new ArrayList<ISmartSwitchable>();
			SmartSwitchableManager.obtainSwitchables(switchables, new WorldLocation(this.world, this.pos), this.placedFace.getOpposite());
			for(EnumFacing face : EnumFacing.VALUES)
				SmartSwitchableManager.obtainSwitchables(switchables, new WorldLocation(this.world, this.pos.offset(face)), face);
			for(ISmartSwitchable sw : switchables)
			{
				if(!sw.canReceive())
					continue;
				sw.setElementActive(active);
			}
		}
	}
	
	@Override
	public boolean isElementActive()
	{
		return this.isActive;
	}
	
	@Override
	protected float getMultipartHardness(EntityPlayer player)
	{
		return 2.0f;
	}
	
	@Override
	public ItemStack getPickBlock(EntityPlayer player)
	{
		return new ItemStack(ItemsPS.SMART_WIRE);
	}
	
	@Override
	public IHandlerProvider getProvider(EnumFacing toFace)
	{
		return this;
	}
	
	@Override
	public <T extends ITileHandler> T getHandler(EnumFacing face, Class<T> type, Object... args)
	{
		if(ISmartSwitchable.class.isAssignableFrom(type))
			return (T) this;
		return null;
	}
	
	@Override
	public <T extends ITileHandler> boolean hasHandler(EnumFacing face, Class<T> type, Object... args)
	{
		return face == this.placedFace && ISmartSwitchable.class.isAssignableFrom(type);
	}
}
