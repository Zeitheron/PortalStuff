package com.zeitheron.portalstuff.multipart;

import com.zeitheron.hammercore.api.multipart.MultipartSignature;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.AxisAlignedBB;

public class MultipartLightBridge extends MultipartSignature
{
	public AxisAlignedBB aabb;
	
	public MultipartLightBridge(AxisAlignedBB aabb)
	{
		this.aabb = aabb;
	}
	
	public MultipartLightBridge()
	{
	}

	@Override
	public AxisAlignedBB getBoundingBox()
	{
		if(this.aabb == null)
		{
			return new AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 1.0, 1.0);
		}
		return this.aabb;
	}

	@Override
	public float getHardness(EntityPlayer player)
	{
		return 5.0f;
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		if(this.aabb != null)
		{
			nbt.setDouble("MinX", this.aabb.minX);
			nbt.setDouble("MinY", this.aabb.minY);
			nbt.setDouble("MinY", this.aabb.minZ);
			nbt.setDouble("MaxX", this.aabb.maxX);
			nbt.setDouble("MaxY", this.aabb.maxY);
			nbt.setDouble("MaxZ", this.aabb.maxZ);
		}
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		if(nbt.hasKey("MinX"))
			this.aabb = new AxisAlignedBB(nbt.getDouble("MinX"), nbt.getDouble("MinY"), nbt.getDouble("MinZ"), nbt.getDouble("MaxX"), nbt.getDouble("MaxY"), nbt.getDouble("MaxZ"));
	}
}