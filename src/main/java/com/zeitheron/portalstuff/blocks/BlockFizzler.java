package com.zeitheron.portalstuff.blocks;

import java.util.HashSet;
import java.util.List;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.hammercore.utils.WorldLocation;
import com.zeitheron.hammercore.utils.math.MathHelper;
import com.zeitheron.portalstuff.Info;
import com.zeitheron.portalstuff.init.BlocksPS;
import com.zeitheron.portalstuff.net.PacketRedifyFizzlers;
import com.zeitheron.portalstuff.tile.TileFizzler;
import com.zeitheron.portalstuff.tile.TileFizzlerPart;

import me.ichun.mods.ichunutil.common.iChunUtil;
import me.ichun.mods.ichunutil.common.grab.GrabHandler;
import me.ichun.mods.portalgun.common.PortalGun;
import me.ichun.mods.portalgun.common.entity.EntityPortalProjectile;
import me.ichun.mods.portalgun.common.item.ItemPortalGun;
import me.ichun.mods.portalgun.common.portal.info.ChannelInfo;
import me.ichun.mods.portalgun.common.portal.info.PortalInfo;
import me.ichun.mods.portalgun.common.portal.world.PortalPlacement;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockFizzler extends Block implements ITileBlock<TileFizzler>, ITileEntityProvider
{
	public BlockFizzler()
	{
		super(Material.IRON);
		this.setTranslationKey("fizzler");
		this.setHardness(4.0f);
	}
	
	@Override
	public Class<TileFizzler> getTileClass()
	{
		return TileFizzler.class;
	}
	
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileFizzler();
	}
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.MODEL;
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		try
		{
			EnumFacing face = (EnumFacing) source.getBlockState(pos).getValue((IProperty) EnumRotation.EFACING);
			if(source.getTileEntity(pos.offset(face.getOpposite())) instanceof TileFizzlerPart)
				return BlockFizzlerPart.EnumFizzlerOrientation.valueOf(face.getAxis().name()).getBoundary();
			double s = 0.4375;
			double e = 0.5625;
			int m = ((EnumFacing) state.getValue((IProperty) EnumRotation.EFACING)).getOpposite().ordinal() - 2;
			return m == 0 ? new AxisAlignedBB(s, 0.0, 0.75, e, 1.0, 1.0) : (m == 1 ? new AxisAlignedBB(s, 0.0, 0.0, e, 1.0, 0.25) : (m == 2 ? new AxisAlignedBB(0.75, 0.0, s, 1.0, 1.0, e) : new AxisAlignedBB(0.0, 0.0, s, 0.25, 1.0, e)));
		} catch(Throwable face)
		{
			return super.getBoundingBox(state, source, pos);
		}
	}
	
	@Override
	public void addCollisionBoxToList(IBlockState state, World worldIn, BlockPos pos, AxisAlignedBB entityBox, List<AxisAlignedBB> collidingBoxes, Entity entityIn, boolean b)
	{
		try
		{
			double s = 0.4375;
			double e = 0.5625;
			int m = ((EnumFacing) state.getValue((IProperty) EnumRotation.EFACING)).getOpposite().ordinal() - 2;
			AxisAlignedBB baseBB = m == 0 ? new AxisAlignedBB(s, 0.0, 0.75, e, 1.0, 1.0) : (m == 1 ? new AxisAlignedBB(s, 0.0, 0.0, e, 1.0, 0.25) : (m == 2 ? new AxisAlignedBB(0.75, 0.0, s, 1.0, 1.0, e) : new AxisAlignedBB(0.0, 0.0, s, 0.25, 1.0, e)));
			BlockFizzler.addCollisionBoxToList((BlockPos) pos, (AxisAlignedBB) entityBox, collidingBoxes, (AxisAlignedBB) baseBB);
			EnumFacing face = (EnumFacing) state.getValue((IProperty) EnumRotation.EFACING);
			if(worldIn.getTileEntity(pos.offset(face.getOpposite())) instanceof TileFizzlerPart && !(entityIn instanceof EntityPlayer))
			{
				BlockFizzler.addCollisionBoxToList((BlockPos) pos, (AxisAlignedBB) entityBox, collidingBoxes, (AxisAlignedBB) BlockFizzlerPart.EnumFizzlerOrientation.valueOf(face.getAxis().name()).getBoundary().grow(-6.25E-5));
			}
		} catch(Throwable s)
		{
			// empty catch block
		}
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public BlockRenderLayer getRenderLayer()
	{
		return BlockRenderLayer.CUTOUT;
	}

	@Override
	public boolean rotateBlock(World w, BlockPos p, EnumFacing s)
	{
		int meta = w.getBlockState(p).getBlock().getMetaFromState(w.getBlockState(p)) + 1;
		if(meta > 5)
		{
			meta = 2;
		}
		w.setBlockState(p, this.getStateFromMeta(meta));
		return true;
	}

	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty((IProperty) EnumRotation.EFACING, (Comparable) EnumFacing.values()[meta]);
	}

	@Override
	public int getMetaFromState(IBlockState state)
	{
		int meta = ((EnumFacing) state.getValue((IProperty) EnumRotation.EFACING)).ordinal();
		return meta;
	}

	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer((Block) this, this.getProperties());
	}
	
	public IProperty[] getProperties()
	{
		return new IProperty[] { EnumRotation.EFACING };
	}
	
	@Override
	public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer, EnumHand hand)
	{
		if(placer == null)
			return super.getStateForPlacement(world, pos, facing, hitX, hitY, hitZ, meta, placer, hand);
		return this.getStateFromMeta(placer.getHorizontalFacing().ordinal());
	}
	
	@Override
	public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase ent, ItemStack stack)
	{
		TileFizzler part = (TileFizzler) new WorldLocation(world, pos).getTileOfType(TileFizzler.class);
		if(part != null)
			part.setElementActive(true);
		else
			BlockFizzler.setPowered(world, pos, true, false, true);
	}
	
	@Override
	public void onEntityCollision(World worldIn, BlockPos pos, IBlockState state, Entity entityIn)
	{
		block17:
		{
			ItemStack stack;
			EntityItem item;
			block18:
			{
				block16:
				{
					int i;
					if(worldIn.isRemote)
					{
						return;
					}
					EnumFacing face = (EnumFacing) worldIn.getBlockState(pos).getValue((IProperty) EnumRotation.EFACING);
					if(!(worldIn.getTileEntity(pos.offset(face.getOpposite())) instanceof TileFizzlerPart))
					{
						return;
					}
					if(!entityIn.getEntityBoundingBox().intersects(this.getBoundingBox(state, worldIn, pos).offset(pos).grow(0.12)))
					{
						return;
					}
					if(!(entityIn instanceof EntityPlayer))
						break block16;
					EntityPlayer player = (EntityPlayer) entityIn;
					GrabHandler handler = GrabHandler.getFirstHandler((EntityLivingBase) player, (Side) Side.SERVER, GrabHandler.class);
					if(handler != null)
					{
						entityIn = handler.grabbed;
						entityIn.setDead();
						HammerCore.audioProxy.playSoundAt(worldIn, Info.MOD_ID + ":fizzler_dispose", pos, 0.2f, 1.2f, SoundCategory.BLOCKS);
						for(i = 0; i < 128; ++i)
						{
							double x = entityIn.getEntityBoundingBox().minX + entityIn.world.rand.nextDouble() * (double) entityIn.width;
							double y = entityIn.getEntityBoundingBox().minY + entityIn.world.rand.nextDouble() * (double) entityIn.height;
							double z = entityIn.getEntityBoundingBox().minZ + entityIn.world.rand.nextDouble() * (double) entityIn.width;
							double cx = entityIn.getEntityBoundingBox().minX + (double) (entityIn.width / 2.0f);
							double cy = entityIn.getEntityBoundingBox().minY + (double) (entityIn.height / 2.0f);
							double cz = entityIn.getEntityBoundingBox().minZ + (double) (entityIn.width / 2.0f);
							double mx = MathHelper.clip((double) (x - cx), (double) -0.1, (double) 0.1);
							double my = MathHelper.clip((double) (y - cy), (double) -0.1, (double) 0.1);
							double mz = MathHelper.clip((double) (z - cz), (double) -0.1, (double) 0.1);
							if(worldIn.rand.nextBoolean())
								HCNet.spawnParticle((World) worldIn, (EnumParticleTypes) EnumParticleTypes.CLOUD, (double) x, (double) y, (double) z, (double) mx, (double) my, (double) mz, (int[]) new int[0]);
							if(worldIn.rand.nextBoolean())
								HCNet.spawnParticle((World) worldIn, (EnumParticleTypes) EnumParticleTypes.END_ROD, (double) x, (double) y, (double) z, (double) (mx * 1.8), (double) (my * 1.8), (double) (mz * 1.8), (int[]) new int[0]);
							if(!worldIn.rand.nextBoolean())
								continue;
							HCNet.spawnParticle((World) worldIn, (EnumParticleTypes) EnumParticleTypes.SMOKE_NORMAL, (double) x, (double) y, (double) z, (double) (mx * 1.8), (double) (my * 1.8), (double) (mz * 1.8), (int[]) new int[0]);
						}
					}
					block1: for(i = 0; i < player.inventory.getSizeInventory(); ++i)
					{
						ItemStack stack2 = player.inventory.getStackInSlot(i);
						if(stack2 == null || !(stack2.getItem() instanceof ItemPortalGun))
							continue;
						HashSet<PortalInfo> infos = PortalGun.eventHandlerServer.getWorldSaveData((int) worldIn.provider.getDimension()).portalList;
						ChannelInfo info = PortalGun.eventHandlerServer.lookupChannel(stack2.getTagCompound().getString("uuid"), stack2.getTagCompound().getString("channelName"));
						for(PortalInfo p1 : infos)
						{
							PortalInfo p2 = p1.getPair();
							if(!p1.uuid.equals(info.uuid) || !p1.channelName.equals(info.channelName))
								continue;
							iChunUtil.proxy.nudgeHand(-15.0f);
							PortalPlacement pp = p1.getPortalPlacement(worldIn);
							if(pp != null)
								worldIn.setBlockToAir(pp.getPos());
							HammerCore.audioProxy.playSoundAt(worldIn, "portalgun:portalgun.wpn_portal_fizzler_shimmy", pos, 3.0f, 1.0f, SoundCategory.BLOCKS);
							continue block1;
						}
					}
					break block17;
				}
				if(!(entityIn instanceof EntityPortalProjectile))
					break block18;
				entityIn.setDead();
				HammerCore.audioProxy.playSoundAt(worldIn, "portalgun:portal.portal_invalid_surface", pos, 3.0f, 1.0f, SoundCategory.BLOCKS);
				HCNet.INSTANCE.sendToAllAround(new PacketRedifyFizzlers(300), new WorldLocation(worldIn, pos).getPointWithRad(64));
				break block17;
			}
			if(!(entityIn instanceof Entity))
				break block17;
			if(entityIn instanceof EntityItem && !InterItemStack.isStackNull((ItemStack) (stack = (item = (EntityItem) entityIn).getItem())) && stack.getItem() == PortalGun.itemPortalGun)
			{
				HashSet<PortalInfo> infos = PortalGun.eventHandlerServer.getWorldSaveData((int) worldIn.provider.getDimension()).portalList;
				ChannelInfo info = PortalGun.eventHandlerServer.lookupChannel(stack.getTagCompound().getString("uuid"), stack.getTagCompound().getString("channelName"));
				for(PortalInfo p1 : infos)
				{
					PortalInfo p2 = p1.getPair();
					if(!p1.uuid.equals(info.uuid) || !p1.channelName.equals(info.channelName))
						continue;
					iChunUtil.proxy.nudgeHand(-15.0f);
					PortalPlacement pp = p1.getPortalPlacement(worldIn);
					if(pp != null)
					{
						worldIn.setBlockToAir(pp.getPos());
					}
					HammerCore.audioProxy.playSoundAt(worldIn, "portalgun:portalgun.wpn_portal_fizzler_shimmy", pos, 3.0f, 1.0f, SoundCategory.BLOCKS);
					break;
				}
				return;
			}
			entityIn.setDead();
			HammerCore.audioProxy.playSoundAt(worldIn, Info.MOD_ID + ":fizzler_dispose", pos, 0.2f, 1.2f, SoundCategory.BLOCKS);
			for(int i = 0; i < 128; ++i)
			{
				double x = entityIn.getEntityBoundingBox().minX + entityIn.world.rand.nextDouble() * (double) entityIn.width;
				double y = entityIn.getEntityBoundingBox().minY + entityIn.world.rand.nextDouble() * (double) entityIn.height;
				double z = entityIn.getEntityBoundingBox().minZ + entityIn.world.rand.nextDouble() * (double) entityIn.width;
				double cx = entityIn.getEntityBoundingBox().minX + (double) (entityIn.width / 2.0f);
				double cy = entityIn.getEntityBoundingBox().minY + (double) (entityIn.height / 2.0f);
				double cz = entityIn.getEntityBoundingBox().minZ + (double) (entityIn.width / 2.0f);
				double mx = MathHelper.clip((double) (x - cx), (double) -0.1, (double) 0.1);
				double my = MathHelper.clip((double) (y - cy), (double) -0.1, (double) 0.1);
				double mz = MathHelper.clip((double) (z - cz), (double) -0.1, (double) 0.1);
				if(worldIn.rand.nextBoolean())
					HCNet.spawnParticle((World) worldIn, (EnumParticleTypes) EnumParticleTypes.CLOUD, (double) x, (double) y, (double) z, (double) mx, (double) my, (double) mz, (int[]) new int[0]);
				if(worldIn.rand.nextBoolean())
					HCNet.spawnParticle((World) worldIn, (EnumParticleTypes) EnumParticleTypes.END_ROD, (double) x, (double) y, (double) z, (double) (mx * 1.8), (double) (my * 1.8), (double) (mz * 1.8), (int[]) new int[0]);
				if(!worldIn.rand.nextBoolean())
					continue;
				HCNet.spawnParticle((World) worldIn, (EnumParticleTypes) EnumParticleTypes.SMOKE_NORMAL, (double) x, (double) y, (double) z, (double) (mx * 1.8), (double) (my * 1.8), (double) (mz * 1.8), (int[]) new int[0]);
			}
		}
	}

	@Override
	public void breakBlock(World world, BlockPos pos, IBlockState state)
	{
		TileFizzler part = (TileFizzler) new WorldLocation(world, pos).getTileOfType(TileFizzler.class);
		if(part != null)
			part.setElementActive(false);
		else
			BlockFizzler.setPowered(world, pos, false, true, false);
		super.breakBlock(world, pos, state);
	}
	
	public static void setPowered(World world, BlockPos pos, boolean powered, boolean updateVertical, boolean playSound)
	{
		IBlockState state = world.getBlockState(pos);
		if(state.getBlock() != BlocksPS.FIZZLER)
			return;
		EnumFacing where = state.getValue(EnumRotation.EFACING).getOpposite();
		BlockPos bpos = pos;
		int mn = updateVertical ? BlockFizzler.getMinFizzlerY(world, bpos) : bpos.getY();
		int mx = updateVertical ? BlockFizzler.getMaxFizzlerY(world, bpos) : bpos.getY();
		boolean played = false;
		for(int y = mn; y < mx + 1; ++y)
		{
			int dist;
			int i;
			pos = new BlockPos(bpos.getX(), y, bpos.getZ());
			if(powered)
			{
				dist = 0;
				for(i = 1; i < 32; ++i)
				{
					BlockPos ori = pos.offset(where, i);
					if(world.getBlockState(ori).getBlock() == BlocksPS.FIZZLER)
					{
						dist = i;
						if(!playSound)
							break;
						played = true;
						break;
					}
					if(world.getBlockState(ori).getBlock().isReplaceable((IBlockAccess) world, ori) || world.getBlockState(ori).getBlock() == BlocksPS.FIZZLER_PART)
						continue;
					return;
				}
				if(dist <= 0)
					continue;
				for(i = 1; i < dist; ++i)
				{
					world.setBlockState(pos.offset(where, i), BlocksPS.FIZZLER_PART.getStateFromMeta(where.getAxis() == EnumFacing.Axis.X ? 1 : 0));
				}
				continue;
			}
			dist = 0;
			for(i = 1; i < 32; ++i)
			{
				if(world.getBlockState(pos.offset(where, i)).getBlock() == BlocksPS.FIZZLER)
				{
					dist = i;
					if(!playSound)
						break;
					played = true;
					break;
				}
				if(world.getBlockState(pos.offset(where, i)).getBlock().isReplaceable((IBlockAccess) world, pos.offset(where, i)) || world.getBlockState(pos.offset(where, i)).getBlock() == BlocksPS.FIZZLER_PART)
					continue;
				return;
			}
			if(dist <= 0)
				continue;
			for(i = 1; i < dist; ++i)
			{
				world.setBlockToAir(pos.offset(where, i));
			}
		}
		if(played)
		{
			HammerCore.audioProxy.playSoundAt(world, Info.MOD_ID + ":fizzler_st" + (powered ? "art" : "op"), pos, 0.3f, 1.0f, SoundCategory.BLOCKS);
		}
	}
	
	public static int getMinFizzlerY(World world, BlockPos pos)
	{
		IBlockState state = world.getBlockState(pos);
		if(state.getBlock() != BlocksPS.FIZZLER)
			return pos.getY();
		while(pos.getY() > 0)
		{
			if(!world.getBlockState(pos).equals(state))
				return pos.getY() + 1;
			pos = pos.down();
		}
		return pos.getY();
	}
	
	public static int getMaxFizzlerY(World world, BlockPos pos)
	{
		IBlockState state = world.getBlockState(pos);
		if(state.getBlock() != BlocksPS.FIZZLER)
			return pos.getY();
		while(pos.getY() < 256)
		{
			if(!world.getBlockState(pos).equals(state))
				return pos.getY() - 1;
			pos = pos.up();
		}
		return pos.getY();
	}
}