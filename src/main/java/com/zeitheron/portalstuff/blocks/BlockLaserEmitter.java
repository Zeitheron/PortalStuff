package com.zeitheron.portalstuff.blocks;

import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.portalstuff.api.smart.ISmartSwitchable;
import com.zeitheron.portalstuff.tile.TileLaserEmitter;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockLaserEmitter extends Block implements ITileBlock<TileLaserEmitter>, ITileEntityProvider
{
	public BlockLaserEmitter()
	{
		super(Material.IRON);
		this.setSoundType(SoundType.METAL);
		this.setTranslationKey("laser_emitter");
	}
	
	@Override
	public Class<TileLaserEmitter> getTileClass()
	{
		return TileLaserEmitter.class;
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		int m = this.getMetaFromState(state) % 4;
		return m == 0 ? new AxisAlignedBB(0.0, 0.0, 0.9375, 1.0, 1.0, 1.0) : (m == 1 ? new AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 1.0, 0.0625) : (m == 2 ? new AxisAlignedBB(0.9375, 0.0, 0.0, 1.0, 1.0, 1.0) : new AxisAlignedBB(0.0, 0.0, 0.0, 0.0625, 1.0, 1.0)));
	}
	
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileLaserEmitter();
	}
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.MODEL;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public BlockRenderLayer getRenderLayer()
	{
		return BlockRenderLayer.TRANSLUCENT;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty(EnumRotation.FACING, EnumRotation.values()[meta % 4]).withProperty((IProperty) ISmartSwitchable.ACTIVE, (Comparable) Boolean.valueOf(meta > 3));
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		int meta = state.getValue(EnumRotation.FACING).ordinal();
		boolean active = state.getValue(ISmartSwitchable.ACTIVE);
		return meta + (active ? 4 : 0);
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer((Block) this, new IProperty[] { EnumRotation.FACING, ISmartSwitchable.ACTIVE });
	}
	
	@Override
	public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer, EnumHand stack)
	{
		if(facing.getAxis() == EnumFacing.Axis.Y)
			facing = placer != null ? placer.getHorizontalFacing() : EnumFacing.NORTH;
		return this.getDefaultState().withProperty((IProperty) EnumRotation.FACING, (Comparable) EnumRotation.valueOf((String) facing.getOpposite().name())).withProperty((IProperty) ISmartSwitchable.ACTIVE, (Comparable) Boolean.valueOf(false));
	}
}