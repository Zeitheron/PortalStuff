package com.zeitheron.portalstuff.blocks;

import java.util.Collections;
import java.util.List;

import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.portalstuff.init.BlocksPS;
import com.zeitheron.portalstuff.tile.TileLightBridgePart;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockLightBridgePart extends BlockContainer implements ITileBlock<TileLightBridgePart>
{
	public BlockLightBridgePart()
	{
		super(Material.CIRCUITS);
		this.setTranslationKey("light_bridge_part");
		this.setSoundType(BlockLightBridge.SOUND_TYPE);
	}
	
	@Override
	public void getSubBlocks(CreativeTabs itemIn, NonNullList<ItemStack> items)
	{
	}
	
	@Override
	public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player)
	{
		if(player.capabilities.isCreativeMode)
		{
			return new ItemStack(BlocksPS.LIGHT_BRIDGE);
		}
		return InterItemStack.NULL_STACK;
	}
	
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileLightBridgePart();
	}
	
	@Override
	public Class<TileLightBridgePart> getTileClass()
	{
		return TileLightBridgePart.class;
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		return NULL_AABB;
	}
	
	@Override
	protected RayTraceResult rayTrace(BlockPos pos, Vec3d start, Vec3d end, AxisAlignedBB boundingBox)
	{
		return null;
	}

	@Override
	public void addCollisionBoxToList(IBlockState state, World worldIn, BlockPos pos, AxisAlignedBB entityBox, List<AxisAlignedBB> collidingBoxes, Entity entityIn, boolean b)
	{
		TileLightBridgePart part = (TileLightBridgePart) ((Object) WorldUtil.cast((Object) worldIn.getTileEntity(pos), TileLightBridgePart.class));
		if(part != null)
		{
			BlockLightBridgePart.addCollisionBoxToList((BlockPos) pos, (AxisAlignedBB) entityBox, collidingBoxes, (AxisAlignedBB) part.aabb);
		}
	}

	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}

	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
	{
		TileLightBridgePart part = (TileLightBridgePart) ((Object) WorldUtil.cast((Object) worldIn.getTileEntity(pos), TileLightBridgePart.class));
		if(part != null && part.towards != null)
		{
			EnumFacing prevt = part.towards;
			BlockPos off = pos.offset(part.towards);
			part = (TileLightBridgePart) ((Object) WorldUtil.cast((Object) worldIn.getTileEntity(off), TileLightBridgePart.class));
			if(part != null && part.towards == prevt)
			{
				part.markRemove = true;
			}
		}
		super.breakBlock(worldIn, pos, state);
	}

	@Override
	public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
	{
		return Collections.emptyList();
	}
}