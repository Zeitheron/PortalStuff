package com.zeitheron.portalstuff.blocks;

import java.util.HashSet;
import java.util.List;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.math.MathHelper;
import com.zeitheron.portalstuff.Info;
import com.zeitheron.portalstuff.blocks.BlockFizzlerPart.EnumFizzlerOrientation;
import com.zeitheron.portalstuff.init.DamageSourcesPS;

import me.ichun.mods.ichunutil.common.iChunUtil;
import me.ichun.mods.portalgun.common.PortalGun;
import me.ichun.mods.portalgun.common.item.ItemPortalGun;
import me.ichun.mods.portalgun.common.portal.info.ChannelInfo;
import me.ichun.mods.portalgun.common.portal.info.PortalInfo;
import me.ichun.mods.portalgun.common.portal.world.PortalPlacement;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.NonNullList;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockLaserFizzlerPart extends Block
{
	public BlockLaserFizzlerPart()
	{
		super(Material.IRON);
		this.setTranslationKey("laser_field");
		this.setBlockUnbreakable();
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		return EnumFizzlerOrientation.values()[(state.getValue(EnumFizzlerOrientation.ORIENTATION).ordinal() + 1) % 2].getBoundary();
	}
	
	@Override
	public AxisAlignedBB getSelectedBoundingBox(IBlockState state, World worldIn, BlockPos pos)
	{
		return EnumFizzlerOrientation.values()[(state.getValue(EnumFizzlerOrientation.ORIENTATION).ordinal() + 1) % 2].getBoundary().offset(pos);
	}
	
	@Override
	public void getSubBlocks(CreativeTabs itemIn, NonNullList<ItemStack> items)
	{
		
	}
	
	@Override
	public void addCollisionBoxToList(IBlockState state, World worldIn, BlockPos pos, AxisAlignedBB entityBox, List<AxisAlignedBB> collidingBoxes, Entity entityIn, boolean p_185477_7_)
	{
		
	}
	
	@Override
	public void onNeighborChange(IBlockAccess worldObj, BlockPos pos, BlockPos neighbor)
	{
	}
	
	@Override
	public void onEntityCollision(World worldIn, BlockPos pos, IBlockState state, Entity entityIn)
	{
		if(worldIn.isRemote)
			return;
		if(!entityIn.getEntityBoundingBox().intersects(this.getBoundingBox(state, (IBlockAccess) worldIn, pos).offset(pos).grow(0.12)))
			return;
		if(entityIn instanceof EntityPlayer)
		{
			int i;
			EntityPlayer player = (EntityPlayer) entityIn;
			if(player.capabilities.isCreativeMode || player.ticksExisted % 2 != 0 || player.getHealth() <= 0.0f)
				return;
			player.attackEntityFrom(DamageSourcesPS.LASER_FIZZLER, 1.0E23F);
			block0: for(i = 0; i < player.inventory.getSizeInventory(); ++i)
			{
				ItemStack stack = player.inventory.getStackInSlot(i);
				if(stack == null || !(stack.getItem() instanceof ItemPortalGun))
					continue;
				HashSet<PortalInfo> infos = PortalGun.eventHandlerServer.getWorldSaveData((int) worldIn.provider.getDimension()).portalList;
				ChannelInfo info = PortalGun.eventHandlerServer.lookupChannel(stack.getTagCompound().getString("uuid"), stack.getTagCompound().getString("channelName"));
				for(PortalInfo p1 : infos)
				{
					PortalInfo p2 = p1.getPair();
					if(!p1.uuid.equals(info.uuid) || !p1.channelName.equals(info.channelName))
						continue;
					iChunUtil.proxy.nudgeHand(-15.0f);
					PortalPlacement pp = p1.getPortalPlacement(worldIn);
					if(pp != null)
					{
						worldIn.setBlockToAir(pp.getPos());
					}
					HammerCore.audioProxy.playSoundAt(worldIn, "portalgun:portalgun.wpn_portal_fizzler_shimmy", pos, 3.0f, 1.0f, SoundCategory.BLOCKS);
					continue block0;
				}
			}
			HammerCore.audioProxy.playSoundAt(worldIn, Info.MOD_ID + ":laser_fizzler_dispose", pos, 0.5f, 1.2f, SoundCategory.BLOCKS);
			for(i = 0; i < 128; ++i)
			{
				double x = entityIn.getEntityBoundingBox().minX + entityIn.world.rand.nextDouble() * (double) entityIn.width;
				double y = entityIn.getEntityBoundingBox().minY + entityIn.world.rand.nextDouble() * (double) entityIn.height;
				double z = entityIn.getEntityBoundingBox().minZ + entityIn.world.rand.nextDouble() * (double) entityIn.width;
				double cx = entityIn.getEntityBoundingBox().minX + (double) (entityIn.width / 2.0f);
				double cy = entityIn.getEntityBoundingBox().minY + (double) (entityIn.height / 2.0f);
				double cz = entityIn.getEntityBoundingBox().minZ + (double) (entityIn.width / 2.0f);
				double mx = MathHelper.clip((double) (x - cx), (double) -0.1, (double) 0.1);
				double my = MathHelper.clip((double) (y - cy), (double) -0.1, (double) 0.1);
				double mz = MathHelper.clip((double) (z - cz), (double) -0.1, (double) 0.1);
				if(worldIn.rand.nextBoolean())
					HCNet.spawnParticle((World) worldIn, (EnumParticleTypes) EnumParticleTypes.CLOUD, (double) x, (double) y, (double) z, (double) mx, (double) my, (double) mz, (int[]) new int[0]);
				if(worldIn.rand.nextBoolean())
					HCNet.spawnParticle((World) worldIn, (EnumParticleTypes) EnumParticleTypes.END_ROD, (double) x, (double) y, (double) z, (double) (mx * 1.8), (double) (my * 1.8), (double) (mz * 1.8), (int[]) new int[0]);
				if(!worldIn.rand.nextBoolean())
					continue;
				HCNet.spawnParticle((World) worldIn, (EnumParticleTypes) EnumParticleTypes.SMOKE_NORMAL, (double) x, (double) y, (double) z, (double) (mx * 1.8), (double) (my * 1.8), (double) (mz * 1.8), (int[]) new int[0]);
			}
		}
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public BlockRenderLayer getRenderLayer()
	{
		return BlockRenderLayer.TRANSLUCENT;
	}
	
	@Override
	public boolean rotateBlock(World w, BlockPos p, EnumFacing s)
	{
		int meta = w.getBlockState(p).getBlock().getMetaFromState(w.getBlockState(p)) + 1;
		if(meta > 3)
		{
			meta = 0;
		}
		w.setBlockState(p, this.getStateFromMeta(meta));
		return true;
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty(BlockFizzlerPart.EnumFizzlerOrientation.ORIENTATION, BlockFizzlerPart.EnumFizzlerOrientation.values()[meta]);
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return state.getValue(EnumFizzlerOrientation.ORIENTATION).ordinal();
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, getProperties());
	}
	
	public IProperty[] getProperties()
	{
		return new IProperty[] { BlockFizzlerPart.EnumFizzlerOrientation.ORIENTATION };
	}
}