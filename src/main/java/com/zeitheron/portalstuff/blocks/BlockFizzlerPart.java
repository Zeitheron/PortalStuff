package com.zeitheron.portalstuff.blocks;

import java.util.HashSet;
import java.util.List;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.InterItemStack;
import com.zeitheron.hammercore.utils.WorldLocation;
import com.zeitheron.hammercore.utils.math.MathHelper;
import com.zeitheron.portalstuff.Info;
import com.zeitheron.portalstuff.api.entity.ICube;
import com.zeitheron.portalstuff.net.PacketRedifyFizzlers;
import com.zeitheron.portalstuff.tile.TileFizzlerPart;

import me.ichun.mods.ichunutil.common.iChunUtil;
import me.ichun.mods.portalgun.common.PortalGun;
import me.ichun.mods.portalgun.common.entity.EntityPortalProjectile;
import me.ichun.mods.portalgun.common.item.ItemPortalGun;
import me.ichun.mods.portalgun.common.portal.info.ChannelInfo;
import me.ichun.mods.portalgun.common.portal.info.PortalInfo;
import me.ichun.mods.portalgun.common.portal.world.PortalPlacement;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockFizzlerPart extends Block implements ITileBlock<TileFizzlerPart>, ITileEntityProvider
{
	public BlockFizzlerPart()
	{
		super(Material.IRON);
		this.setTranslationKey("fizzler_part");
		this.setBlockUnbreakable();
	}
	
	@Override
	public Class<TileFizzlerPart> getTileClass()
	{
		return TileFizzlerPart.class;
	}
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		return (state.getValue(EnumFizzlerOrientation.ORIENTATION) == EnumFizzlerOrientation.X ? EnumFizzlerOrientation.Z : EnumFizzlerOrientation.X).getBoundary();
	}
	
	@Override
	public void getSubBlocks(CreativeTabs itemIn, NonNullList<ItemStack> items)
	{
	}
	
	@Override
	public void addCollisionBoxToList(IBlockState state, World worldIn, BlockPos pos, AxisAlignedBB entityBox, List<AxisAlignedBB> collidingBoxes, Entity entityIn, boolean b)
	{
		if(!(entityIn instanceof EntityPlayer))
			addCollisionBoxToList(pos, entityBox, collidingBoxes, this.getBoundingBox(state, (IBlockAccess) worldIn, pos).grow(-6.25E-5));
	}

	@Override
	public void onNeighborChange(IBlockAccess worldObj, BlockPos pos, BlockPos neighbor)
	{
	}

	@Override
	public void onEntityCollision(World worldIn, BlockPos pos, IBlockState state, Entity entityIn)
	{
		block16:
		{
			ItemStack stack;
			EntityItem item;
			block17:
			{
				block15:
				{
					int i;
					if(worldIn.isRemote)
						return;
					if(!entityIn.getEntityBoundingBox().intersects(this.getBoundingBox(state, (IBlockAccess) worldIn, pos).offset(pos).grow(0.12)))
						return;
					if(!(entityIn instanceof EntityPlayer))
						break block15;
					EntityPlayer player = (EntityPlayer) entityIn;
					block1: for(i = 0; i < player.inventory.getSizeInventory(); ++i)
					{
						ItemStack stack2 = player.inventory.getStackInSlot(i);
						if(stack2 == null || !(stack2.getItem() instanceof ItemPortalGun))
							continue;
						HashSet<PortalInfo> infos = PortalGun.eventHandlerServer.getWorldSaveData((int) worldIn.provider.getDimension()).portalList;
						ChannelInfo info = PortalGun.eventHandlerServer.lookupChannel(stack2.getTagCompound().getString("uuid"), stack2.getTagCompound().getString("channelName"));
						for(PortalInfo p1 : infos)
						{
							PortalInfo p2 = p1.getPair();
							if(!p1.uuid.equals(info.uuid) || !p1.channelName.equals(info.channelName))
								continue;
							iChunUtil.proxy.nudgeHand(-15.0f);
							PortalPlacement pp = p1.getPortalPlacement(worldIn);
							if(pp != null)
								worldIn.setBlockToAir(pp.getPos());
							HammerCore.audioProxy.playSoundAt(worldIn, "portalgun:portalgun.wpn_portal_fizzler_shimmy", pos, 3.0f, 1.0f, SoundCategory.BLOCKS);
							continue block1;
						}
					}
					break block16;
				}
				if(!(entityIn instanceof EntityPortalProjectile))
					break block17;
				entityIn.setDead();
				HammerCore.audioProxy.playSoundAt(worldIn, "portalgun:portal.portal_invalid_surface", pos, 3.0f, 1.0f, SoundCategory.BLOCKS);
				HCNet.INSTANCE.sendToAllAround(new PacketRedifyFizzlers(300), new WorldLocation(worldIn, pos).getPointWithRad(64));
				break block16;
			}
			if(!(entityIn instanceof Entity))
				break block16;
			if(entityIn instanceof EntityItem && !InterItemStack.isStackNull((ItemStack) (stack = (item = (EntityItem) entityIn).getItem())) && stack.getItem() == PortalGun.itemPortalGun)
			{
				HashSet<PortalInfo> infos = PortalGun.eventHandlerServer.getWorldSaveData((int) worldIn.provider.getDimension()).portalList;
				ChannelInfo info = PortalGun.eventHandlerServer.lookupChannel(stack.getTagCompound().getString("uuid"), stack.getTagCompound().getString("channelName"));
				for(PortalInfo p1 : infos)
				{
					PortalInfo p2 = p1.getPair();
					if(!p1.uuid.equals(info.uuid) || !p1.channelName.equals(info.channelName))
						continue;
					iChunUtil.proxy.nudgeHand(-15.0f);
					PortalPlacement pp = p1.getPortalPlacement(worldIn);
					if(pp != null)
						worldIn.setBlockToAir(pp.getPos());
					HammerCore.audioProxy.playSoundAt(worldIn, "portalgun:portalgun.wpn_portal_fizzler_shimmy", pos, 3.0f, 1.0f, SoundCategory.BLOCKS);
					break;
				}
				return;
			}
			HammerCore.audioProxy.playSoundAt(worldIn, Info.MOD_ID + ":fizzler_dispose", pos, 0.2f, 1.2f, SoundCategory.BLOCKS);
			if(!(entityIn instanceof ICube))
				for(int i = 0; i < 128; ++i)
				{
					double x = entityIn.getEntityBoundingBox().minX + entityIn.world.rand.nextDouble() * entityIn.width;
					double y = entityIn.getEntityBoundingBox().minY + entityIn.world.rand.nextDouble() * entityIn.height;
					double z = entityIn.getEntityBoundingBox().minZ + entityIn.world.rand.nextDouble() * entityIn.width;
					double cx = entityIn.getEntityBoundingBox().minX + entityIn.width / 2;
					double cy = entityIn.getEntityBoundingBox().minY + entityIn.height / 2;
					double cz = entityIn.getEntityBoundingBox().minZ + entityIn.width / 2;
					double mx = MathHelper.clip(x - cx, -.1, .1);
					double my = MathHelper.clip(y - cy, -.1, .1);
					double mz = MathHelper.clip(z - cz, -.1, .1);
					if(worldIn.rand.nextBoolean())
						HCNet.spawnParticle(worldIn, EnumParticleTypes.CLOUD, x, y, z, mx, my, mz);
					if(worldIn.rand.nextBoolean())
						HCNet.spawnParticle(worldIn, EnumParticleTypes.END_ROD, x, y, z, mx * 1.8, my * 1.8, mz * 1.8);
					if(!worldIn.rand.nextBoolean())
						continue;
					HCNet.spawnParticle(worldIn, EnumParticleTypes.SMOKE_NORMAL, x, y, z, mx * 1.8, my * 1.8, mz * 1.8);
				}
			entityIn.setDead();
		}
	}

	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public BlockRenderLayer getRenderLayer()
	{
		return BlockRenderLayer.TRANSLUCENT;
	}

	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.ENTITYBLOCK_ANIMATED;
	}

	@Override
	public boolean rotateBlock(World w, BlockPos p, EnumFacing s)
	{
		int meta = w.getBlockState(p).getBlock().getMetaFromState(w.getBlockState(p)) + 1;
		if(meta > 3)
		{
			meta = 0;
		}
		w.setBlockState(p, this.getStateFromMeta(meta));
		return true;
	}

	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty(EnumFizzlerOrientation.ORIENTATION, EnumFizzlerOrientation.values()[meta]);
	}

	@Override
	public int getMetaFromState(IBlockState state)
	{
		int meta = ((EnumFizzlerOrientation) ((Object) state.getValue(EnumFizzlerOrientation.ORIENTATION))).ordinal();
		return meta;
	}

	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer((Block) this, this.getProperties());
	}
	
	public IProperty[] getProperties()
	{
		return new IProperty[] { EnumFizzlerOrientation.ORIENTATION };
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileFizzlerPart();
	}
	
	public static enum EnumFizzlerOrientation implements IStringSerializable
	{
		X(new Vec3d(0.0, 0.0, 0.5), new Vec3d(1.0, 1.0, 0.5)), Z(new Vec3d(0.5, 0.0, 0.0), new Vec3d(0.5, 1.0, 1.0));
		
		public static final PropertyEnum<EnumFizzlerOrientation> ORIENTATION;
		private final AxisAlignedBB aabb;
		
		private EnumFizzlerOrientation(Vec3d a, Vec3d b)
		{
			this.aabb = new AxisAlignedBB(a.x, a.y, a.z, b.x, b.y, b.z);
		}
		
		public AxisAlignedBB getBoundary()
		{
			return this.aabb;
		}

		@Override
		public String getName()
		{
			return this.name().toLowerCase();
		}
		
		static
		{
			ORIENTATION = PropertyEnum.create("orientation", EnumFizzlerOrientation.class);
		}
	}
}