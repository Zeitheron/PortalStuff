package com.zeitheron.portalstuff.blocks;

import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.portalstuff.api.smart.ISmartSwitchable;
import com.zeitheron.portalstuff.tile.TileFloorButtonBase;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockFloorButtonBase extends Block implements ITileBlock<TileFloorButtonBase>, ITileEntityProvider
{
	public BlockFloorButtonBase()
	{
		super(Material.IRON);
		this.setSoundType(SoundType.METAL);
		this.setHardness(20.0f);
		this.setTranslationKey("floor_button_base");
	}

	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		return new AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.1640625, 1.0);
	}

	@Override
	public TileFloorButtonBase createNewTileEntity(World worldIn, int meta)
	{
		return new TileFloorButtonBase();
	}

	@Override
	public Class<TileFloorButtonBase> getTileClass()
	{
		return TileFloorButtonBase.class;
	}

	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, ISmartSwitchable.ACTIVE);
	}

	@Override
	public int getMetaFromState(IBlockState state)
	{
		return state.getValue(ISmartSwitchable.ACTIVE) ? 1 : 0;
	}

	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty(ISmartSwitchable.ACTIVE, meta == 1);
	}

	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.MODEL;
	}

	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
}