package com.zeitheron.portalstuff.blocks;

import java.util.List;

import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.portalstuff.events.LightBridgeObtainer;
import com.zeitheron.portalstuff.init.SoundsPS;
import com.zeitheron.portalstuff.tile.TileLightBridge;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.SoundEvents;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockLightBridge extends BlockContainer implements ITileBlock<TileLightBridge>
{
	public static final SoundType SOUND_TYPE = new SoundType(1.0f, 0.9f, SoundsPS.DESTRUCTION_LIGHTBRIDGE_ELEC.sound, SoundsPS.FOOTSTEP_LIGHT_BRIDGE.sound, SoundEvents.BLOCK_GLASS_PLACE, SoundsPS.FOOTSTEP_LIGHT_BRIDGE.sound, SoundsPS.FOOTSTEP_LIGHT_BRIDGE.sound);
	
	public BlockLightBridge()
	{
		super(Material.IRON);
		this.setSoundType(SOUND_TYPE);
		this.setTranslationKey("light_bridge");
	}

	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		TileLightBridge t = WorldUtil.cast(source.getTileEntity(pos), TileLightBridge.class);
		if(t != null)
		{
			double nx = 0.0;
			double xx = 1.0;
			double ny = (double) t.height - 0.125;
			double xy = (double) t.height + 0.1875;
			double nz = 0.0;
			double xz = 1.0;
			if(t.face == EnumFacing.NORTH)
			{
				nz = 0.875;
				nx = 0.0625;
				xx = 0.9375;
			}
			if(t.face == EnumFacing.SOUTH)
			{
				xz = 0.125;
				nx = 0.0625;
				xx = 0.9375;
			}
			if(t.face == EnumFacing.WEST)
			{
				nx = 0.875;
				nz = 0.0625;
				xz = 0.9375;
			}
			if(t.face == EnumFacing.EAST)
			{
				xx = 0.125;
				nz = 0.0625;
				xz = 0.9375;
			}
			return new AxisAlignedBB(nx, ny, nz, xx, xy, xz);
		}
		return FULL_BLOCK_AABB;
	}
	
	@Override
	public void addCollisionBoxToList(IBlockState state, World worldIn, BlockPos pos, AxisAlignedBB entityBox, List<AxisAlignedBB> collidingBoxes, Entity entityIn, boolean p_185477_7_)
	{
		float h = 0.5f;
		TileLightBridge tile = WorldUtil.cast(worldIn.getTileEntity(pos), TileLightBridge.class);
		if(tile != null && tile.isElementActive())
		{
			h = tile.height;
			BlockLightBridge.addCollisionBoxToList(pos, entityBox, collidingBoxes, new AxisAlignedBB(0.0, (double) h, 0.0, 1.0, (double) h, 1.0));
		}
		super.addCollisionBoxToList(state, worldIn, pos, entityBox, collidingBoxes, entityIn, p_185477_7_);
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		TileLightBridge tile = LightBridgeObtainer.pull();
		if(tile == null)
		{
			tile = new TileLightBridge();
			tile.face = EnumFacing.NORTH;
			tile.height = 0.5f;
		}
		return tile;
	}

	@Override
	public Class<TileLightBridge> getTileClass()
	{
		return TileLightBridge.class;
	}

	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}

	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.ENTITYBLOCK_ANIMATED;
	}

	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
	{
		TileLightBridge t = (TileLightBridge) WorldUtil.cast((Object) worldIn.getTileEntity(pos), TileLightBridge.class);
		if(t != null && t.isElementActive())
			worldIn.setBlockToAir(pos.offset(t.face));
		super.breakBlock(worldIn, pos, state);
	}

	@Override
	public boolean canPlaceBlockOnSide(World worldIn, BlockPos pos, EnumFacing side)
	{
		return side.getAxis() != EnumFacing.Axis.Y && super.canPlaceBlockOnSide(worldIn, pos, side);
	}

	@Override
	public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
	{
		TileLightBridge t = new TileLightBridge();
		t.height = hitY;
		t.face = facing;
		if(placer.isSneaking())
			t.height = 0.5f;
		LightBridgeObtainer.push(t);
		return super.getStateForPlacement(worldIn, pos, facing, hitX, hitY, hitZ, meta, placer);
	}
}