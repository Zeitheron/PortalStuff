package com.zeitheron.portalstuff.virtual.portalplacers;

import com.zeitheron.hammercore.api.IProcess;
import com.zeitheron.portalstuff.api.smart.ISmartSwitchable;

import me.ichun.mods.portalgun.common.PortalGun;
import me.ichun.mods.portalgun.common.portal.PortalGunHelper;
import me.ichun.mods.portalgun.common.portal.info.ChannelInfo;
import me.ichun.mods.portalgun.common.portal.info.PortalInfo;
import me.ichun.mods.portalgun.common.portal.world.PortalsInWorldSavedData;
import me.ichun.mods.portalgun.common.tileentity.TilePortalBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class PortalPlacer implements ISmartSwitchable
{
	public String uuid;
	public String channel;
	public final BlockPos[] positions = new BlockPos[2];
	public boolean isActive;
	public EnumFacing placed;
	public EnumFacing up;
	public World world;
	public boolean isTypeA;
	
	public PortalPlacer(String uuid, String channel)
	{
		this.uuid = uuid;
		this.channel = channel;
	}
	
	public PortalPlacer(NBTTagCompound nbt)
	{
		this.readFromNBT(nbt);
	}
	
	@Override
	public void setElementActive(boolean active)
	{
		if(this.isActive == active)
			return;
		this.isActive = active;
		if(active)
			new TaskPlacePortal(1).update();
		else
		{
			int tempts = 20;
			while(tempts-- > 0)
			{
				int c = 0;
				for(BlockPos pos : this.positions)
				{
					if(!(this.world.getTileEntity(pos) instanceof TilePortalBase) || c++ <= 0)
						continue;
					this.world.setBlockToAir(pos);
				}
				if(c <= 0)
					continue;
				break;
			}
		}
	}
	
	@Override
	public boolean isElementActive()
	{
		return this.isActive;
	}
	
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		nbt.setString("uuid", this.uuid);
		nbt.setString("channel", this.channel);
		nbt.setIntArray("pos1", new int[] { this.positions[0].getX(), this.positions[0].getY(), this.positions[0].getZ() });
		nbt.setIntArray("pos2", new int[] { this.positions[1].getX(), this.positions[1].getY(), this.positions[1].getZ() });
		nbt.setBoolean("active", this.isActive);
		nbt.setString("face", this.placed.name().toLowerCase());
		if(this.up != null)
		{
			nbt.setString("up", this.up.name().toLowerCase());
		}
		nbt.setBoolean("typeA", this.isTypeA);
		return nbt;
	}
	
	public PortalPlacer readFromNBT(NBTTagCompound nbt)
	{
		this.uuid = nbt.getString("uuid");
		this.channel = nbt.getString("channel");
		this.isActive = nbt.getBoolean("active");
		int[] p1 = nbt.getIntArray("pos1");
		int[] p2 = nbt.getIntArray("pos2");
		this.positions[0] = new BlockPos(p1[0], p1[1], p1[2]);
		this.positions[1] = new BlockPos(p2[0], p2[1], p2[2]);
		this.placed = EnumFacing.valueOf((String) nbt.getString("face").toUpperCase());
		if(nbt.hasKey("up"))
		{
			this.up = EnumFacing.valueOf((String) nbt.getString("up").toUpperCase());
		}
		this.isTypeA = nbt.getBoolean("typeA");
		return this;
	}
	
	public class TaskPlacePortal implements IProcess
	{
		public int ticksLeft;
		
		public TaskPlacePortal(int cooldown)
		{
			this.ticksLeft = 0;
			this.ticksLeft = cooldown;
		}
		
		public void update()
		{
			--this.ticksLeft;
			if(this.ticksLeft <= 0)
			{
				int tempts = 20;
				while(tempts-- > 0)
				{
					try
					{
						PortalInfo pi;
						ChannelInfo ci;
						PortalsInWorldSavedData po;
						if(PortalPlacer.this.up != null)
						{
							po = PortalGun.eventHandlerServer.getWorldSaveData(PortalPlacer.this.world.provider.getDimension());
							ci = po.getChannel(PortalPlacer.this.uuid, PortalPlacer.this.channel);
							pi = new PortalInfo();
							pi.uuid = ci.uuid;
							pi.channelName = ci.channelName;
							pi.colour = PortalPlacer.this.isTypeA ? ci.colourA : ci.colourB;
							pi.isTypeA = PortalPlacer.this.isTypeA;
							BlockPos pos = PortalPlacer.this.positions[1].offset(PortalPlacer.this.placed.getOpposite());
							if(PortalPlacer.this.up.getAxis() == EnumFacing.Axis.X)
							{
								pos = pos.offset(PortalPlacer.this.up.getOpposite());
							}
							if(!PortalGunHelper.spawnPortal((World) PortalPlacer.this.world, (BlockPos) pos, (EnumFacing) PortalPlacer.this.placed, (EnumFacing) PortalPlacer.this.up, (PortalInfo) pi, (int) 1, (int) 2, (boolean) true))
								continue;
						}
						po = PortalGun.eventHandlerServer.getWorldSaveData(PortalPlacer.this.world.provider.getDimension());
						ci = po.getChannel(PortalPlacer.this.uuid, PortalPlacer.this.channel);
						pi = new PortalInfo();
						pi.uuid = ci.uuid;
						pi.channelName = ci.channelName;
						pi.colour = PortalPlacer.this.isTypeA ? ci.colourA : ci.colourB;
						pi.isTypeA = PortalPlacer.this.isTypeA;
						if(!PortalGunHelper.spawnPortal((World) PortalPlacer.this.world, (BlockPos) PortalPlacer.this.positions[1].offset(PortalPlacer.this.placed.getOpposite()), (EnumFacing) PortalPlacer.this.placed, (EnumFacing) PortalPlacer.this.placed, (PortalInfo) pi, (int) 1, (int) 2, (boolean) true))
							continue;
						break;
					} catch(Throwable po)
					{
					}
				}
			}
		}
		
		public boolean isAlive()
		{
			return this.ticksLeft > 0;
		}
	}
	
}
