package com.zeitheron.portalstuff.virtual.portalplacers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import com.zeitheron.hammercore.annotations.MCFBus;
import com.zeitheron.hammercore.lib.zlib.utils.IndexedMap;
import com.zeitheron.hammercore.net.HCNet;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.IWorldEventListener;
import net.minecraft.world.World;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

@MCFBus
public class PortalPlacerListManager
{
	public static final PortalPlacerList visiblePlacerList = new PortalPlacerList();
	private static Map<Integer, PortalPlacerList> lists = new IndexedMap();
	
	public static PortalPlacerList getPlacersListFor(World world)
	{
		if(world.isRemote)
		{
			return visiblePlacerList;
		}
		PortalPlacerList data = lists.get(world.provider.getDimension());
		if(data == null)
		{
			data = new PortalPlacerList();
			MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
			String worldSave = world.provider.getDimension() == 0 ? "data" : "DIM" + world.provider.getDimension() + File.separator + "data";
			File file = new File((server.isDedicatedServer() ? "" : new StringBuilder().append("saves").append(File.separator).toString()) + server.getFolderName(), worldSave + File.separator + "PortalStuffPortalPlacers.data");
			try
			{
				FileInputStream fileinputstream = new FileInputStream(file);
				data.readFromNBT(CompressedStreamTools.readCompressed((InputStream) fileinputstream));
				fileinputstream.close();
			} catch(Throwable fileinputstream)
			{
				// empty catch block
			}
			data.setWorld(world);
		}
		return data;
	}
	
	@SubscribeEvent
	public void dimensionLoad(WorldEvent.Load e)
	{
		World world = e.getWorld();
		if(world.isRemote)
		{
			return;
		}
		PortalPlacerList data = new PortalPlacerList();
		MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
		String worldSave = world.provider.getDimension() == 0 ? "data" : "DIM" + world.provider.getDimension() + File.separator + "data";
		File file = new File((server.isDedicatedServer() ? "" : new StringBuilder().append("saves").append(File.separator).toString()) + server.getFolderName(), worldSave + File.separator + "PortalStuffPortalPlacers.data");
		try
		{
			FileInputStream fileinputstream = new FileInputStream(file);
			data.readFromNBT(CompressedStreamTools.readCompressed((InputStream) fileinputstream));
			fileinputstream.close();
		} catch(Throwable fileinputstream)
		{
			// empty catch block
		}
		lists.put(world.provider.getDimension(), data);
		data.setWorld(world);
		world.addEventListener((IWorldEventListener) new WorldListenerPortalPlacer(world));
	}
	
	@SubscribeEvent
	public void playerJoin(PlayerEvent.PlayerLoggedInEvent e)
	{
		if(e.player instanceof EntityPlayerMP)
		{
			PortalPlacerListManager.sendVisiblePlacersToPlayer((EntityPlayerMP) e.player);
		}
	}
	
	@SubscribeEvent
	public void dimensionSave(WorldEvent.Save e)
	{
		World world = e.getWorld();
		if(world.isRemote)
		{
			return;
		}
		PortalPlacerList data = PortalPlacerListManager.getPlacersListFor(world);
		NBTTagCompound nbt = data.writeToNBT(new NBTTagCompound());
		MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
		String worldSave = world.provider.getDimension() == 0 ? "data" : "DIM" + world.provider.getDimension() + File.separator + "data";
		File file = new File((server.isDedicatedServer() ? "" : new StringBuilder().append("saves").append(File.separator).toString()) + server.getFolderName(), worldSave + File.separator + "PortalStuffPortalPlacers.data");
		try
		{
			FileOutputStream fileoutputstream = new FileOutputStream(file);
			CompressedStreamTools.writeCompressed((NBTTagCompound) nbt, (OutputStream) fileoutputstream);
			fileoutputstream.close();
		} catch(Throwable err)
		{
			err.printStackTrace();
		}
	}
	
	public static void sendVisiblePlacersToPlayer(EntityPlayerMP player)
	{
		PortalPlacerList list = new PortalPlacerList();
		PortalPlacerList world = PortalPlacerListManager.getPlacersListFor(player.world);
		for(PortalPlacer p : world.getPlacers())
		{
			if(Math.sqrt(player.getDistanceSqToCenter(p.positions[0])) > 64.0)
				continue;
			list.setPlacer(p);
		}
		HCNet.INSTANCE.sendTo(new PacketSendPortalPlacers(list), player);
	}
}
