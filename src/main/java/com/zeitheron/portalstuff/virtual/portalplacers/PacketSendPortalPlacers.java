package com.zeitheron.portalstuff.virtual.portalplacers;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;

import net.minecraft.nbt.NBTTagCompound;

public class PacketSendPortalPlacers implements IPacket
{
	private NBTTagCompound nbt;
	
	public PacketSendPortalPlacers(PortalPlacerList ppl)
	{
		this.nbt = ppl.writeToNBT(new NBTTagCompound());
	}
	
	public PacketSendPortalPlacers()
	{
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setTag("t", this.nbt);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.nbt = nbt.getCompoundTag("t");
	}
	
	@Override
	public IPacket executeOnClient(PacketContext net)
	{
		PortalPlacerListManager.visiblePlacerList.readFromNBT(this.nbt);
		return null;
	}
}
