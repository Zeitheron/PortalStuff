package com.zeitheron.portalstuff.virtual.portalplacers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.storage.WorldSavedData;

public class PortalPlacerList extends WorldSavedData
{
	private Map<Long, Integer> mappings = new HashMap<Long, Integer>();
	private List<PortalPlacer> placers = new ArrayList<PortalPlacer>();
	public World world;
	
	public PortalPlacerList(String name)
	{
		super("PortalStuffPortalPlacers");
	}
	
	public PortalPlacerList()
	{
		super("PortalStuffPortalPlacers");
	}
	
	public List<PortalPlacer> getPlacers()
	{
		return placers;
	}
	
	public void setWorld(World world)
	{
		this.world = world;
		for(PortalPlacer p : placers)
			p.world = world;
	}
	
	public boolean removePlacer(BlockPos pos)
	{
		return removePlacer(getPlacer(pos));
	}
	
	public boolean removePlacer(PortalPlacer placer)
	{
		if(placer != null)
		{
			if(!placers.contains(placer))
				return false;
			for(BlockPos p : placer.positions)
				mappings.remove(p.toLong());
			int start = placers.indexOf(placer);
			placers.remove(start);
			for(int i = start; i < this.placers.size(); ++i)
			{
				PortalPlacer p = this.placers.get(i);
				for(BlockPos p2 : p.positions)
					mappings.put(p2.toLong(), i);
			}
			return true;
		}
		return false;
	}
	
	public boolean setPlacer(PortalPlacer placer)
	{
		for(BlockPos p$ : placer.positions)
		{
			if(getPlacer(p$) == null)
				continue;
			return false;
		}
		placers.add(placer);
		int i = placers.indexOf(placer);
		for(BlockPos p : placer.positions)
			mappings.put(p.toLong(), i);
		placer.world = world;
		return true;
	}
	
	public PortalPlacer getPlacer(BlockPos pos)
	{
		Integer i = mappings.get(pos.toLong());
		if(i != null)
			return placers.get(i);
		return null;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		placers.clear();
		mappings.clear();
		NBTTagList placers = nbt.getTagList("PortalPlacers", 10);
		int count = nbt.getInteger("PlacerCount");
		for(int i = 0; i < count; ++i)
			setPlacer(new PortalPlacer(placers.getCompoundTagAt(i)));
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		nbt.setInteger("PlacerCount", placers.size());
		NBTTagList list = new NBTTagList();
		for(PortalPlacer placer : this.getPlacers())
			list.appendTag(placer.writeToNBT(new NBTTagCompound()));
		nbt.setTag("PortalPlacers", list);
		return nbt;
	}
}