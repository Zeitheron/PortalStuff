package com.zeitheron.portalstuff.gui.inv;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.portalstuff.api.tile.ctx.IContextProperty;
import com.zeitheron.portalstuff.api.tile.ctx.IContextTile;
import com.zeitheron.portalstuff.api.tile.ctx.IDataReceiver;
import com.zeitheron.portalstuff.api.tile.ctx.IDataSender;
import com.zeitheron.portalstuff.net.PacketSendCTXData;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.fml.relauncher.Side;

public class ContainerContextEditor extends Container
{
	public final IContextTile tile;
	public final List<IContextProperty> properties = new ArrayList<IContextProperty>();
	public final IDataSender sender;
	
	public ContainerContextEditor(IContextTile tile, IDataSender sender)
	{
		this.tile = tile;
		tile.addProperties(this.properties);
		this.sender = sender;
	}
	
	public void accept(PacketSendCTXData data, Side side)
	{
		String name = data.getName();
		NBTTagCompound comp = data.getData();
		for(IContextProperty prop : this.properties)
		{
			IDataReceiver rec;
			if(!prop.name().equals(name) || (rec = prop.getDataReceiver()) == null)
				continue;
			rec.receiveData(comp);
			this.tile.apply(prop);
			if(side != Side.SERVER)
				break;
			this.sender.sendData(prop, comp);
			break;
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn)
	{
		TileEntity tile = this.tile.asTile();
		return tile != null ? tile.getPos().distanceSq(playerIn.getPosition()) <= 64.0 : true;
	}
}
