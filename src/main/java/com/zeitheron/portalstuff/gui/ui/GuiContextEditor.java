package com.zeitheron.portalstuff.gui.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.portalstuff.Info;
import com.zeitheron.portalstuff.api.tile.ctx.ICTXRender;
import com.zeitheron.portalstuff.api.tile.ctx.IContextProperty;
import com.zeitheron.portalstuff.api.tile.ctx.IContextTile;
import com.zeitheron.portalstuff.api.tile.ctx.IDataSender;
import com.zeitheron.portalstuff.gui.inv.ContainerContextEditor;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;

public class GuiContextEditor extends GuiContainer
{
	public final ContainerContextEditor editor;
	private final List<ICTXRender> renders = new ArrayList<ICTXRender>();
	private ICTXRender currentHover;
	private final List<String> tooltip = new ArrayList<String>();
	
	public GuiContextEditor(IContextTile tile)
	{
		super((Container) new ContainerContextEditor(tile, IDataSender.DefaultDataSenderToServer));
		this.editor = (ContainerContextEditor) this.inventorySlots;
		this.ySize = 0;
		this.xSize = 0;
		for(int i = 0; i < this.editor.properties.size(); ++i)
		{
			IContextProperty prop = this.editor.properties.get(i);
			ICTXRender render = prop.getClientHandler();
			this.renders.add(render);
			this.xSize = Math.max(this.xSize, render.width());
			this.ySize += render.height();
			render.setSender(IDataSender.DefaultDataSenderToServer);
		}
		HammerCore.audioProxy.playSoundAt((World) Minecraft.getMinecraft().world, Info.MOD_ID + ":ctx_editor.open", Minecraft.getMinecraft().player.getPosition(), 1.0f, 1.0f, SoundCategory.MASTER);
		Keyboard.enableRepeatEvents((boolean) true);
	}

	@Override
	public void onGuiClosed()
	{
		super.onGuiClosed();
		HammerCore.audioProxy.playSoundAt((World) Minecraft.getMinecraft().world, Info.MOD_ID + ":ctx_editor.close", Minecraft.getMinecraft().player.getPosition(), 1.0f, 1.0f, SoundCategory.MASTER);
		Keyboard.enableRepeatEvents((boolean) false);
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTicks);
		if(!this.tooltip.isEmpty())
		{
			this.drawHoveringText(this.tooltip, mouseX, mouseY);
			this.tooltip.clear();
		}
		renderHoveredToolTip(mouseX, mouseY);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		boolean found = false;
		int y = this.guiTop;
		for(int i = 0; i < this.renders.size(); ++i)
		{
			ICTXRender render = this.renders.get(i);
			int width = render.width();
			int height = render.height();
			int left = (this.width - width) / 2;
			GL11.glPushMatrix();
			GL11.glTranslatef((float) left, (float) y, (float) 0.0f);
			render.render(mouseX - left, mouseY - y);
			GL11.glPopMatrix();
			if(mouseX >= left && mouseX < left + width && mouseY >= y && mouseY < y + height)
			{
				if(render != this.currentHover)
				{
					HammerCore.audioProxy.playSoundAt((World) Minecraft.getMinecraft().world, Info.MOD_ID + ":ctx_editor.select", Minecraft.getMinecraft().player.getPosition(), 1.0f, 1.0f, SoundCategory.MASTER);
				}
				found = true;
				this.currentHover = render;
				render.getTooltip(this.tooltip, mouseX - left, mouseY - y);
			}
			y += height;
		}
		if(!found)
		{
			this.currentHover = null;
		}
	}

	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		int y = this.guiTop;
		for(int i = 0; i < this.renders.size(); ++i)
		{
			ICTXRender render = this.renders.get(i);
			int width = render.width();
			int height = render.height();
			int left = (this.width - width) / 2;
			if(render == this.currentHover)
			{
				render.mouseClicked(mouseX - left, mouseY - y);
			}
			y += height;
		}
	}

	@Override
	protected void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick)
	{
		super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick);
		int y = this.guiTop;
		for(int i = 0; i < this.renders.size(); ++i)
		{
			ICTXRender render = this.renders.get(i);
			int width = render.width();
			int height = render.height();
			int left = (this.width - width) / 2;
			if(render == this.currentHover)
			{
				render.mouseDragged(mouseX - left, mouseY - y);
			}
			y += height;
		}
	}

	@Override
	protected void mouseReleased(int mouseX, int mouseY, int state)
	{
		super.mouseReleased(mouseX, mouseY, state);
		int y = this.guiTop;
		for(int i = 0; i < this.renders.size(); ++i)
		{
			ICTXRender render = this.renders.get(i);
			int width = render.width();
			int height = render.height();
			int left = (this.width - width) / 2;
			if(render == this.currentHover)
			{
				render.mouseReleased(mouseX - left, mouseY - y);
			}
			y += height;
		}
	}

	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		if(keyCode == 1)
		{
			this.mc.player.closeScreen();
		}
		int y = this.guiTop;
		for(int i = 0; i < this.renders.size(); ++i)
		{
			ICTXRender render = this.renders.get(i);
			int width = render.width();
			int height = render.height();
			int left = (this.width - width) / 2;
			if(render == this.currentHover)
			{
				render.keyPressed(typedChar, keyCode);
			}
			y += height;
		}
	}
}
