package com.zeitheron.portalstuff.gui;

import java.util.List;

import com.zeitheron.hammercore.utils.WorldLocation;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.portalstuff.api.tile.ctx.IContextProperty;
import com.zeitheron.portalstuff.api.tile.ctx.IContextTile;
import com.zeitheron.portalstuff.api.tile.ctx.IDataSender;
import com.zeitheron.portalstuff.api.tile.ctx.builtin.bool.ContextPropertyBoolean;
import com.zeitheron.portalstuff.api.tile.ctx.builtin.str.ContextPropertyString;
import com.zeitheron.portalstuff.gui.inv.ContainerContextEditor;
import com.zeitheron.portalstuff.gui.ui.GuiContextEditor;
import com.zeitheron.portalstuff.virtual.portalplacers.PortalPlacer;
import com.zeitheron.portalstuff.virtual.portalplacers.PortalPlacerListManager;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class GuiManagerPS implements IGuiHandler
{
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		PortalPlacer placer;
		IContextTile tile;
		WorldLocation loc = new WorldLocation(world, new BlockPos(x, y, z));
		if(ID == 0 && player instanceof EntityPlayerMP && (tile = (IContextTile) WorldUtil.cast((Object) loc.getTile(), IContextTile.class)) != null)
			return new ContainerContextEditor(tile, IDataSender.toPlayer((EntityPlayerMP) player));
		if(ID == 1 && player instanceof EntityPlayerMP && (placer = PortalPlacerListManager.getPlacersListFor(world).getPlacer(new BlockPos(x, y, z))) != null)
		{
			IContextTile tile2 = new IContextTile()
			{
				@Override
				public void apply(IContextProperty prop)
				{
					if(prop.name().equals("portalplacer.orange") && prop instanceof ContextPropertyBoolean)
					{
						boolean bl = placer.isTypeA = (Boolean) prop.get() == false;
					}
					if(prop.name().equals("portalplacer.uuid") && prop instanceof ContextPropertyString)
					{
						placer.uuid = prop.get() + "";
					}
					if(prop.name().equals("portalplacer.channel") && prop instanceof ContextPropertyString)
					{
						placer.channel = prop.get() + "";
					}
				}
				
				@Override
				public void addProperties(List<IContextProperty> props)
				{
					ContextPropertyBoolean bluePortal = new ContextPropertyBoolean("portalplacer.orange");
					bluePortal.set(!placer.isTypeA);
					props.add(bluePortal);
					ContextPropertyString str = new ContextPropertyString("portalplacer.uuid");
					str.set(placer.uuid);
					props.add(str);
					str = new ContextPropertyString("portalplacer.channel");
					str.set(placer.channel);
					props.add(str);
				}
			};
			return new ContainerContextEditor(tile2, IDataSender.toPlayer((EntityPlayerMP) player));
		}
		return null;
	}

	@Override
	@SideOnly(value = Side.CLIENT)
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		PortalPlacer placer;
		IContextTile tile;
		WorldLocation loc = new WorldLocation(world, new BlockPos(x, y, z));
		if(ID == 0 && (tile = WorldUtil.cast(loc.getTile(), IContextTile.class)) != null)
			return new GuiContextEditor(tile);
		if(ID == 1 && (placer = PortalPlacerListManager.getPlacersListFor(world).getPlacer(new BlockPos(x, y, z))) != null)
		{
			IContextTile tile2 = new IContextTile()
			{
				
				@Override
				public void apply(IContextProperty prop)
				{
					if(prop.name().equals("portalplacer.orange") && prop instanceof ContextPropertyBoolean)
					{
						boolean bl = placer.isTypeA = (Boolean) prop.get() == false;
					}
					if(prop.name().equals("portalplacer.uuid") && prop instanceof ContextPropertyString)
					{
						placer.uuid = prop.get() + "";
					}
					if(prop.name().equals("portalplacer.channel") && prop instanceof ContextPropertyString)
					{
						placer.channel = prop.get() + "";
					}
				}
				
				@Override
				public void addProperties(List<IContextProperty> props)
				{
					ContextPropertyBoolean bluePortal = new ContextPropertyBoolean("portalplacer.orange");
					bluePortal.set(!placer.isTypeA);
					props.add(bluePortal);
					ContextPropertyString str = new ContextPropertyString("portalplacer.uuid");
					str.set(placer.uuid);
					props.add(str);
					str = new ContextPropertyString("portalplacer.channel");
					str.set(placer.channel);
					props.add(str);
				}
			};
			return new GuiContextEditor(tile2);
		}
		return null;
	}
}