package com.zeitheron.portalstuff.tile;

import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.utils.WorldLocation;
import com.zeitheron.portalstuff.init.BlocksPS;

import me.ichun.mods.portalgun.common.tileentity.TilePortalBase;
import net.minecraft.block.Block;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;

public class TileLightBridgePart extends TileSyncableTickable
{
	public AxisAlignedBB aabb;
	public EnumFacing towards;
	public int length = 0;
	public boolean markRemove = false;
	
	public TileLightBridgePart(AxisAlignedBB aabb, EnumFacing facedTowards, int len)
	{
		this.aabb = aabb;
		this.towards = facedTowards;
		this.length = len;
	}
	
	public TileLightBridgePart()
	{
		this.aabb = new AxisAlignedBB(0.0, 0.5, 0.0, 1.0, 0.5, 1.0);
		this.length = 7;
	}
	
	@Override
	public void tick()
	{
		if(towards == null)
			return;
		
		WorldLocation nloc = getLocation().offset(towards);
		
		if(world.isRemote)
			return;
		
		if(markRemove)
		{
			getLocation().setAir();
		} else if(this.length > 0 && this.towards != null && (nloc.getBlock().isReplaceable(world, nloc.getPos()) || nloc.getTile() instanceof TilePortalBase))
		{
			nloc.setState(BlocksPS.LIGHT_BRIDGE_PART.getDefaultState(), 3);
			nloc.setTile(new TileLightBridgePart(this.aabb, this.towards, this.length - 1));
		}
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		if(nbt.hasKey("MinX"))
			this.aabb = new AxisAlignedBB(nbt.getDouble("MinX"), nbt.getDouble("MinY"), nbt.getDouble("MinZ"), nbt.getDouble("MaxX"), nbt.getDouble("MaxY"), nbt.getDouble("MaxZ"));
		this.towards = EnumFacing.VALUES[nbt.getInteger("Facing")];
		this.length = nbt.getInteger("Length");
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		if(this.towards != null)
		{
			nbt.setInteger("Facing", this.towards.ordinal());
		}
		nbt.setInteger("Length", this.length);
		if(this.aabb != null)
		{
			nbt.setDouble("MinX", this.aabb.minX);
			nbt.setDouble("MinY", this.aabb.minY);
			nbt.setDouble("MinZ", this.aabb.minZ);
			nbt.setDouble("MaxX", this.aabb.maxX);
			nbt.setDouble("MaxY", this.aabb.maxY);
			nbt.setDouble("MaxZ", this.aabb.maxZ);
		}
	}
	
	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		return Block.FULL_BLOCK_AABB.offset(this.pos);
	}
}