package com.zeitheron.portalstuff.tile;

import java.util.List;
import java.util.Map;

import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.portalstuff.api.smart.ISmartSwitchable;
import com.zeitheron.portalstuff.api.tile.ctx.IContextProperty;
import com.zeitheron.portalstuff.api.tile.ctx.IContextTile;
import com.zeitheron.portalstuff.api.tile.ctx.builtin.bool.ContextPropertyBoolean;
import com.zeitheron.portalstuff.blocks.BlockFizzler;
import com.zeitheron.portalstuff.init.BlocksPS;

import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;

public class TileFizzler extends TileSyncable implements ISmartSwitchable, IContextTile
{
	public boolean inverted;
	
	@Override
	public boolean isElementActive()
	{
		return false;
	}
	
	@Override
	public void setElementActive(boolean active)
	{
		BlockFizzler.setPowered(this.world, this.pos, this.inverted ? active : !active, true, true);
	}

	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setBoolean("Inverted", this.inverted);
	}

	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		this.inverted = nbt.getBoolean("Inverted");
	}

	@Override
	public void addProperties(Map<String, Object> properties, RayTraceResult trace)
	{
		properties.put("inverted", this.inverted);
	}
	
	@Override
	public void addProperties(List<IContextProperty> props)
	{
		ContextPropertyBoolean prop = new ContextPropertyBoolean("inverted");
		prop.set(this.inverted);
		props.add(prop);
	}
	
	@Override
	public void apply(IContextProperty prop)
	{
		if(prop.name().equals("inverted") && prop instanceof ContextPropertyBoolean)
		{
			this.inverted = (Boolean) prop.get();
			int mn = BlockFizzler.getMinFizzlerY(this.world, this.pos);
			int mx = BlockFizzler.getMaxFizzlerY(this.world, this.pos);
			NBTTagCompound nbt = new NBTTagCompound();
			this.writeNBT(nbt);
			block0: for(int y = mn; y <= mx; ++y)
			{
				IBlockState state;
				BlockPos pos = new BlockPos(this.pos.getX(), y, this.pos.getZ());
				TileFizzler fizzler = (TileFizzler) WorldUtil.cast((Object) this.world.getTileEntity(pos), TileFizzler.class);
				if(fizzler != null)
				{
					fizzler.readNBT(nbt);
				}
				if((state = this.world.getBlockState(pos)).getBlock() != BlocksPS.FIZZLER)
					continue;
				EnumFacing where = ((EnumFacing) state.getValue((IProperty) EnumRotation.EFACING)).getOpposite();
				for(int i = 1; i < 32; ++i)
				{
					BlockPos ori = pos.offset(where, i);
					if(this.world.getBlockState(ori).getBlock() != BlocksPS.FIZZLER)
						continue;
					fizzler = (TileFizzler) WorldUtil.cast((Object) this.world.getTileEntity(ori), TileFizzler.class);
					if(fizzler == null)
						continue block0;
					fizzler.readNBT(nbt);
					continue block0;
				}
			}
			BlockFizzler.setPowered(this.world, this.pos, !this.inverted, true, true);
		}
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		return this.getLocation().getAABB();
	}
}