package com.zeitheron.portalstuff.tile;

import java.util.List;

import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.WorldLocation;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.portalstuff.api.smart.ISmartSwitchable;
import com.zeitheron.portalstuff.api.tile.ctx.IContextProperty;
import com.zeitheron.portalstuff.api.tile.ctx.IContextTile;
import com.zeitheron.portalstuff.api.tile.ctx.builtin.bool.ContextPropertyBoolean;
import com.zeitheron.portalstuff.init.BlocksPS;

import me.ichun.mods.portalgun.common.tileentity.TilePortalBase;
import net.minecraft.block.Block;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;

public class TileLightBridge extends TileSyncable implements ISmartSwitchable, IContextTile
{
	public EnumFacing face;
	public float height;
	public boolean inverted = false;
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setInteger("Facing", this.face.ordinal());
		nbt.setFloat("Height", this.height);
		nbt.setBoolean("Inverted", this.inverted);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		this.face = EnumFacing.VALUES[nbt.getInteger("Facing")];
		this.height = nbt.getFloat("Height");
		this.inverted = nbt.getBoolean("Inverted");
	}
	
	@Override
	public void setElementActive(boolean active)
	{
		if(this.inverted)
			active = !active;
		
		if(active != isElementActive())
		{
			WorldLocation l = getLocation().offset(face);
			
			if(active && (l.getBlock().isReplaceable(world, l.getPos()) || l.getTile() instanceof TilePortalBase))
			{
				l.setState(BlocksPS.LIGHT_BRIDGE_PART.getDefaultState());
				l.setTile(new TileLightBridgePart(new AxisAlignedBB(0, height, 0, 1, height, 1), face, 127));
			} else if(!active)
				l.setAir();
		}
	}
	
	@Override
	public boolean isElementActive()
	{
		TileLightBridgePart part = WorldUtil.cast(world.getTileEntity(pos.offset(face)), TileLightBridgePart.class);
		return this.face != null && part != null && part.towards != null && part.towards.equals(this.face);
	}
	
	@Override
	public void addProperties(List<IContextProperty> props)
	{
		ContextPropertyBoolean e = new ContextPropertyBoolean("inverted");
		e.set(inverted);
		props.add(e);
	}
	
	@Override
	public void apply(IContextProperty prop)
	{
		if(prop.name().equals("inverted") && prop instanceof ContextPropertyBoolean)
		{
			setElementActive(!isElementActive());
			inverted = (Boolean) prop.get();
		}
	}
	
	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		return Block.FULL_BLOCK_AABB.offset(this.pos);
	}
}