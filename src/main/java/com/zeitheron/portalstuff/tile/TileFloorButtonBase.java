package com.zeitheron.portalstuff.tile;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.net.props.NetPropertyNumber;
import com.zeitheron.hammercore.tile.ITileDroppable;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.portalstuff.Info;
import com.zeitheron.portalstuff.api.entity.ICube;
import com.zeitheron.portalstuff.api.smart.ISmartSwitchable;
import com.zeitheron.portalstuff.api.smart.SmartSwitchableManager;
import com.zeitheron.portalstuff.init.BlocksPS;

import net.minecraft.block.properties.IProperty;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class TileFloorButtonBase extends TileSyncableTickable implements ISmartSwitchable, ITileDroppable
{
	public boolean isActive;
	public final NetPropertyNumber<Float> pressure;
	
	public TileFloorButtonBase()
	{
		this.pressure = new NetPropertyNumber(this, 0F);
	}

	@Override
	public void tick()
	{
		boolean newActive;
		try
		{
			this.isActive = (Boolean) this.getWorld().getBlockState(this.getPos()).getValue((IProperty) ACTIVE);
		} catch(Throwable throwable)
		{
		}
		if(this.world.isRemote)
			return;
		List ent = world.getEntitiesWithinAABB(Entity.class, getLocation().getAABB().expand(.3, .25, .3).expand(-.1, 0, -.1), e -> !e.isDead && ((e instanceof EntityPlayer && !((EntityPlayer) e).isSpectator()) || e instanceof ICube));
		float press = pressure.get().floatValue();
		press = !ent.isEmpty() ? (press += 0.1f) : (press -= 0.1f);
		pressure.set(Float.valueOf(MathHelper.clamp(press, 0.0f, 1.0f)));
		boolean prevActive = this.isActive;
		boolean bl = newActive = !ent.isEmpty();
		if(prevActive != newActive)
		{
			this.setElementActive(newActive);
			HammerCore.audioProxy.playSoundAt(this.world, Info.MOD_ID + ":floor_button_" + (newActive ? "down" : "up"), this.pos, 0.2f, 1.0f, SoundCategory.BLOCKS);
			this.getLocation().setTile((TileEntity) this);
			this.validate();
		}
		try
		{
			ArrayList<ISmartSwitchable> switchables = new ArrayList<ISmartSwitchable>();
			for(EnumFacing face : EnumFacing.VALUES)
				SmartSwitchableManager.obtainSwitchables(switchables, this.loc.offset(face), face);
			for(ISmartSwitchable sw : switchables)
			{
				if(!sw.canReceive())
					continue;
				sw.setElementActive(newActive);
			}
		} catch(Throwable switchables)
		{
			// empty catch block
		}
	}
	
	@Override
	public boolean isElementActive()
	{
		return this.isActive;
	}
	
	@Override
	public void setElementActive(boolean active)
	{
		this.getLocation().setState(BlocksPS.FLOOR_BUTTON_BASE.getDefaultState().withProperty(ACTIVE, active));
	}
	
	@Override
	public boolean canReceive()
	{
		return false;
	}

	@Override
	public void writeNBT(NBTTagCompound arg0)
	{
		arg0.setFloat("pressure", this.pressure.get());
	}

	@Override
	public void readNBT(NBTTagCompound arg0)
	{
		this.pressure.set(arg0.getFloat("pressure"));
	}

	@Override
	public void createDrop(EntityPlayer player, World world, BlockPos pos)
	{
		world.removeTileEntity(pos);
	}
}