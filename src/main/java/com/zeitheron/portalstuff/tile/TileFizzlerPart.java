package com.zeitheron.portalstuff.tile;

import com.zeitheron.hammercore.tile.TileSyncable;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.AxisAlignedBB;

public class TileFizzlerPart extends TileSyncable
{
	@Override
	public void readNBT(NBTTagCompound arg0)
	{
	}

	@Override
	public void writeNBT(NBTTagCompound arg0)
	{
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		return this.getLocation().getAABB();
	}
}