package com.zeitheron.portalstuff.tile;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.hammercore.utils.VecDir;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.portalstuff.Info;
import com.zeitheron.portalstuff.api.entity.ICube;
import com.zeitheron.portalstuff.api.entity.ILaserTriggerable;
import com.zeitheron.portalstuff.api.smart.ISmartSwitchable;
import com.zeitheron.portalstuff.api.tile.ctx.IContextProperty;
import com.zeitheron.portalstuff.api.tile.ctx.IContextTile;
import com.zeitheron.portalstuff.api.tile.ctx.builtin.bool.ContextPropertyBoolean;
import com.zeitheron.portalstuff.init.DamageSourcesPS;
import com.zeitheron.portalstuff.utils.Provider;

import me.ichun.mods.ichunutil.common.module.worldportals.common.portal.WorldPortal;
import me.ichun.mods.portalgun.common.portal.world.PortalPlacement;
import me.ichun.mods.portalgun.common.tileentity.TilePortalBase;
import net.minecraft.block.properties.IProperty;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

public class TileLaserEmitter extends TileSyncableTickable implements ISmartSwitchable, IContextTile
{
	public final LaserInfo info;
	public boolean isActive;
	public boolean inverted;
	
	public TileLaserEmitter()
	{
		this.info = new LaserInfo();
		this.inverted = false;
	}

	@Override
	public void tick()
	{
		this.isActive = (Boolean) this.getWorld().getBlockState(this.getPos()).getValue((IProperty) ACTIVE);
		this.info.update();
		this.info.redefine();
	}
	
	@Override
	public void setElementActive(boolean active)
	{
		if(this.inverted)
			active = !active;
		if(active && !this.isActive)
			HammerCore.audioProxy.playSoundAt(this.getWorld(), Info.MOD_ID + ":laser_activation", this.pos, 1.0f, 1.0f, SoundCategory.BLOCKS);
		if(!active && this.isActive)
			HammerCore.audioProxy.playSoundAt(this.getWorld(), Info.MOD_ID + ":laser_node_power_off", this.pos, 1.0f, 1.0f, SoundCategory.BLOCKS);
		this.getLocation().setState(this.loc.getState().withProperty((IProperty) ACTIVE, (Comparable) Boolean.valueOf(active)));
		this.getLocation().getTile().deserializeNBT(this.serializeNBT());
	}
	
	@Override
	public boolean isElementActive()
	{
		return this.isActive;
	}

	@Override
	public void readNBT(NBTTagCompound arg0)
	{
		this.inverted = arg0.getBoolean("Inverted");
	}

	@Override
	public void writeNBT(NBTTagCompound arg0)
	{
		arg0.setBoolean("Inverted", this.inverted);
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		return INFINITE_EXTENT_AABB;
	}

	@Override
	public double getMaxRenderDistanceSquared()
	{
		return 65536.0;
	}
	
	@Override
	public void addProperties(List<IContextProperty> props)
	{
		ContextPropertyBoolean prop = new ContextPropertyBoolean("inverted");
		prop.set(this.inverted);
		props.add(prop);
	}
	
	@Override
	public void apply(IContextProperty prop)
	{
		if(prop.name().equals("inverted") && prop instanceof ContextPropertyBoolean)
		{
			this.inverted = (Boolean) prop.get();
			this.setElementActive(this.isActive);
			this.sync();
		}
	}
	
	public class LaserInfo
	{
		public List<VecDir> dirs;
		private List<ILaserTriggerable> triggerables;
		
		public LaserInfo()
		{
			this.dirs = new ArrayList<VecDir>();
			this.triggerables = new ArrayList<ILaserTriggerable>();
		}
		
		public void redefine()
		{
			this.dirs.clear();
			EnumFacing shootFace = EnumFacing.valueOf((String) ((EnumRotation) TileLaserEmitter.this.getWorld().getBlockState(TileLaserEmitter.this.getPos()).getValue((IProperty) EnumRotation.FACING)).name()).getOpposite();
			double distLeft = 40.0;
			VecDir dir = new VecDir(new Vec3d((Vec3i) TileLaserEmitter.this.pos).add(0.5, 0.5, 0.5), new Vec3d(shootFace.getDirectionVec()), distLeft);
			RayTraceResult res = TileLaserEmitter.this.world.rayTraceBlocks(dir.start, dir.calcEndVec());
			if(res != null && res.hitVec != null)
			{
				PortalPlacement place;
				dir = new VecDir(dir.start, dir.direction, res.hitVec.distanceTo(dir.start));
				TilePortalBase portal = (TilePortalBase) WorldUtil.cast((Object) TileLaserEmitter.this.getWorld().getTileEntity(res.getBlockPos().offset(res.sideHit)), TilePortalBase.class);
				if(portal != null && (place = (PortalPlacement) portal.portals.get((Object) res.sideHit)) != null)
				{
					boolean zero = place.portalSpots[0].equals((Object) res.getBlockPos().offset(res.sideHit));
					WorldPortal wp = place.getPair();
					if(wp != null)
					{
						Vec3d vec = wp.getPosition();
						EnumFacing face = wp.getFaceOn();
						this.dirs.add(dir);
						dir = new VecDir(vec, new Vec3d(face.getDirectionVec()), distLeft -= dir.distance);
					}
				}
			}
			Provider exclude = new Provider();
			int cycles = 20;
			while(distLeft > 0.0 && cycles-- > 0)
			{
				EnumFacing face;
				Vec3d vec;
				WorldPortal wp;
				PortalPlacement place;
				TilePortalBase portal;
				Entity ent = dir.getClosestEntityWithinDir(TileLaserEmitter.this.getWorld(), Entity.class, e -> e.canBeCollidedWith() && e != exclude.t);
				if(ent != null)
				{
					dir = dir.cutTill(ent);
					res = TileLaserEmitter.this.world.rayTraceBlocks(dir.start, dir.calcEndVec());
					if(res != null && res.hitVec != null)
					{
						dir = new VecDir(dir.start, dir.direction, res.hitVec.distanceTo(dir.start));
						portal = (TilePortalBase) WorldUtil.cast((Object) TileLaserEmitter.this.getWorld().getTileEntity(res.getBlockPos().offset(res.sideHit)), TilePortalBase.class);
						if(portal != null && (place = (PortalPlacement) portal.portals.get((Object) res.sideHit)) != null && (wp = place.getPair()) != null)
						{
							vec = wp.getPosition();
							face = wp.getFaceOn();
							this.dirs.add(dir);
							dir = new VecDir(vec, new Vec3d(face.getDirectionVec()), distLeft -= dir.distance);
						}
					}
					this.dirs.add(dir);
					if(!(ent instanceof ILaserTriggerable))
						break;
					dir = new VecDir(ent.getPositionVector().add(0.0, (double) (ent.height / 2.0f), 0.0), 0.0f, ent.rotationYaw, distLeft -= dir.distance);
					exclude.t = ent;
					continue;
				}
				res = TileLaserEmitter.this.world.rayTraceBlocks(dir.start, dir.calcEndVec());
				if(res != null && res.hitVec != null)
				{
					dir = new VecDir(dir.start, dir.direction, res.hitVec.distanceTo(dir.start));
					portal = (TilePortalBase) WorldUtil.cast((Object) TileLaserEmitter.this.getWorld().getTileEntity(res.getBlockPos().offset(res.sideHit)), TilePortalBase.class);
					if(portal != null && (place = (PortalPlacement) portal.portals.get((Object) res.sideHit)) != null && (wp = place.getPair()) != null)
					{
						vec = wp.getPosition();
						face = wp.getFaceOn();
						this.dirs.add(dir);
						dir = new VecDir(vec, new Vec3d(face.getDirectionVec()), distLeft -= dir.distance);
					}
				}
				this.dirs.add(dir);
				break;
			}
		}
		
		public void update()
		{
			ILaserTriggerable trigger;
			int i;
			this.triggerables.clear();
			for(i = 0; i < this.dirs.size(); ++i)
			{
				trigger = (ILaserTriggerable) this.dirs.get(i).getClosestEntityWithinDir(TileLaserEmitter.this.world, Entity.class, e -> e instanceof ILaserTriggerable);
				if(trigger == null)
					continue;
				this.triggerables.add(trigger);
			}
			this.redefine();
			if(TileLaserEmitter.this.isActive)
			{
				this.hurtMobs();
			}
			for(i = 0; i < this.dirs.size(); ++i)
			{
				trigger = (ILaserTriggerable) this.dirs.get(i).getClosestEntityWithinDir(TileLaserEmitter.this.world, Entity.class, e -> e instanceof ILaserTriggerable);
				if(trigger == null)
					continue;
				trigger.toggle(TileLaserEmitter.this.isActive);
				this.triggerables.remove(trigger);
			}
			for(ILaserTriggerable t : this.triggerables)
			{
				t.toggle(false);
			}
		}
		
		public void hurtMobs()
		{
			for(int i = 0; i < this.dirs.size(); ++i)
			{
				Entity ent = this.dirs.get(i).getClosestEntityWithinDir(TileLaserEmitter.this.world, Entity.class, e -> !(e instanceof ILaserTriggerable) && !(e instanceof ICube));
				if(ent == null || !ent.attackEntityFrom(DamageSourcesPS.LASER, 4.0f))
					continue;
				HammerCore.audioProxy.playSoundAt(TileLaserEmitter.this.world, Info.MOD_ID + ":laser_hurt_mob", ent.getPosition(), 1.0f, 1.0f, SoundCategory.NEUTRAL);
			}
		}
	}
	
}
