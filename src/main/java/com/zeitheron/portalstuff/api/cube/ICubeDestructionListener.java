package com.zeitheron.portalstuff.api.cube;

import com.zeitheron.portalstuff.api.entity.ICube;

public interface ICubeDestructionListener
{
	public void onDestructed(ICube var1);
}