package com.zeitheron.portalstuff.api.tile.ctx.builtin.bool;

import com.zeitheron.portalstuff.api.tile.ctx.ICTXRender;
import com.zeitheron.portalstuff.api.tile.ctx.IContextProperty;
import com.zeitheron.portalstuff.api.tile.ctx.IDataReceiver;

import net.minecraft.nbt.NBTTagByte;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ContextPropertyBoolean implements IContextProperty<Boolean, NBTTagByte>, IDataReceiver
{
	public final String name;
	public boolean value;
	
	public ContextPropertyBoolean(String name)
	{
		this.name = name;
	}
	
	@Override
	public String name()
	{
		return this.name;
	}
	
	@Override
	public Boolean get()
	{
		return this.value;
	}
	
	@Override
	public void set(Boolean val)
	{
		this.value = val == Boolean.TRUE;
	}
	
	@Override
	public NBTTagByte serialize()
	{
		return new NBTTagByte((byte) (this.value ? 1 : 0));
	}
	
	@Override
	public void deserialize(NBTTagByte nbt)
	{
		this.value = nbt.getByte() == 1;
	}
	
	@SideOnly(value = Side.CLIENT)
	@Override
	public ICTXRender getClientHandler()
	{
		return new CTXBooleanRender(this);
	}
	
	@Override
	public IDataReceiver getDataReceiver()
	{
		return this;
	}
	
	@Override
	public void receiveData(NBTTagCompound nbt)
	{
		this.value = nbt.getBoolean("v");
	}
}