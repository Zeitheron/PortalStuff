package com.zeitheron.portalstuff.api.tile.ctx;

import java.util.List;

public interface ICTXRender
{
	public int width();
	
	public int height();
	
	default public void mouseClicked(int mouseX, int mouseY)
	{
	}
	
	default public void mouseDragged(int mouseX, int mouseY)
	{
	}
	
	default public void mouseReleased(int mouseX, int mouseY)
	{
	}
	
	public void render(int var1, int var2);
	
	default public void getTooltip(List<String> tooltip, int mouseX, int mouseY)
	{
	}
	
	default public void keyPressed(char typedChar, int keyCode)
	{
	}
	
	default public void mouseEntered()
	{
	}
	
	default public void mouseExited()
	{
	}
	
	public IContextProperty property();
	
	public void setSender(IDataSender var1);
}