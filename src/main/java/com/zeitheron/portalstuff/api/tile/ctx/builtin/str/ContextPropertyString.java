package com.zeitheron.portalstuff.api.tile.ctx.builtin.str;

import com.zeitheron.portalstuff.api.tile.ctx.ICTXRender;
import com.zeitheron.portalstuff.api.tile.ctx.IContextProperty;
import com.zeitheron.portalstuff.api.tile.ctx.IDataReceiver;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagString;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ContextPropertyString implements IContextProperty<String, NBTTagString>, IDataReceiver
{
	public final String name;
	public String value;
	
	public ContextPropertyString(String name)
	{
		this.name = name;
	}
	
	@Override
	public String name()
	{
		return this.name;
	}
	
	@Override
	public String get()
	{
		return this.value;
	}
	
	@Override
	public void set(String val)
	{
		this.value = val + "";
	}
	
	@Override
	public NBTTagString serialize()
	{
		return new NBTTagString(this.value);
	}
	
	@Override
	public void deserialize(NBTTagString nbt)
	{
		this.value = nbt.getString();
	}
	
	@SideOnly(value = Side.CLIENT)
	@Override
	public ICTXRender getClientHandler()
	{
		return new CTXStringRender(this);
	}
	
	@Override
	public IDataReceiver getDataReceiver()
	{
		return this;
	}
	
	@Override
	public void receiveData(NBTTagCompound nbt)
	{
		this.value = nbt.getString("v");
	}
}
