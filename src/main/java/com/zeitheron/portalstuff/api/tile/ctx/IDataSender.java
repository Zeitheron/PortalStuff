package com.zeitheron.portalstuff.api.tile.ctx;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.portalstuff.net.PacketSendCTXData;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;

public interface IDataSender
{
	public static final IDataSender DefaultDataSenderToServer = (prop, tag) -> HCNet.INSTANCE.sendToServer(new PacketSendCTXData(prop.name(), tag));
	
	public static IDataSender toPlayer(EntityPlayerMP mp)
	{
		return (prop, tag) -> HCNet.INSTANCE.sendTo(new PacketSendCTXData(prop.name(), tag), mp);
	}
	
	public void sendData(IContextProperty var1, NBTTagCompound var2);
}