package com.zeitheron.portalstuff.api.tile.ctx;

import java.util.List;

import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.tileentity.TileEntity;

public interface IContextTile
{
	public void addProperties(List<IContextProperty> var1);
	
	public void apply(IContextProperty var1);
	
	default public TileEntity asTile()
	{
		return WorldUtil.cast(this, TileEntity.class);
	}
}