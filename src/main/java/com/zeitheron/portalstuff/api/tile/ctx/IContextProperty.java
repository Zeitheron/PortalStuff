package com.zeitheron.portalstuff.api.tile.ctx;

import net.minecraft.nbt.NBTBase;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public interface IContextProperty<TYPE, NBT extends NBTBase>
{
	public TYPE get();
	
	public void set(TYPE var1);
	
	public NBT serialize();
	
	public void deserialize(NBT var1);
	
	public String name();
	
	@SideOnly(value = Side.CLIENT)
	public ICTXRender getClientHandler();
	
	public IDataReceiver getDataReceiver();
}