package com.zeitheron.portalstuff.api.tile.ctx.builtin.str;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.utils.color.ColorARGB;
import com.zeitheron.portalstuff.Info;
import com.zeitheron.portalstuff.api.tile.ctx.ICTXRender;
import com.zeitheron.portalstuff.api.tile.ctx.IContextProperty;
import com.zeitheron.portalstuff.api.tile.ctx.IDataSender;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.SoundCategory;

public class CTXStringRender implements ICTXRender
{
	public final ContextPropertyString prop;
	public IDataSender sender;
	public GuiTextField tf;
	
	public CTXStringRender(ContextPropertyString prop)
	{
		this.prop = prop;
		this.tf = new GuiTextField(0, Minecraft.getMinecraft().fontRenderer, 4, 16, this.width() - 8, 20);
		this.tf.setText(prop.get());
		this.tf.setMaxStringLength(10000);
	}
	
	@Override
	public int width()
	{
		FontRenderer f = Minecraft.getMinecraft().fontRenderer;
		ScaledResolution s = new ScaledResolution(Minecraft.getMinecraft());
		int wid = Math.max(f.getStringWidth(I18n.format("context.string." + this.prop.name()) + ": "), f.getStringWidth(this.prop.get())) + 4;
		return Math.min(wid, s.getScaledWidth());
	}
	
	@Override
	public int height()
	{
		return 45;
	}
	
	@Override
	public void mouseClicked(int mouseX, int mouseY)
	{
		this.tf.mouseClicked(mouseX, mouseY, 1);
	}
	
	@Override
	public void render(int mouseX, int mouseY)
	{
		int w = this.width();
		int h = this.height();
		boolean hovered = mouseX >= 0 && mouseX < this.width() && mouseY >= 0 && mouseY < h;
		GlStateManager.disableTexture2D();
		ColorARGB.glColourRGB((int) (!hovered ? 11184810 : 10066329));
		RenderUtil.drawTexturedModalRect((double) 0.0, (double) 0.0, (double) 0.0, (double) 0.0, (double) w, (double) h);
		GlStateManager.enableTexture2D();
		Minecraft.getMinecraft().fontRenderer.drawString(I18n.format("context.string." + this.prop.name()) + ": ", 2, 2, 0);
		this.tf.width = this.width() - 8;
		this.tf.drawTextBox();
	}
	
	@Override
	public void keyPressed(char typedChar, int keyCode)
	{
		boolean changed;
		String len = this.tf.getText();
		this.tf.textboxKeyTyped(typedChar, keyCode);
		boolean bl = changed = !len.equals(this.tf.getText());
		if(changed)
		{
			NBTTagCompound v = new NBTTagCompound();
			v.setString("v", this.tf.getText());
			this.sender.sendData(this.prop, v);
			this.prop.set(this.tf.getText());
			HammerCore.audioProxy.playSoundAt(Minecraft.getMinecraft().world, Info.MOD_ID + ":ctx_editor.toggle", Minecraft.getMinecraft().player.getPosition(), 1.0f, 1.0f, SoundCategory.MASTER);
		}
	}
	
	@Override
	public void setSender(IDataSender sender)
	{
		this.sender = sender;
	}
	
	@Override
	public IContextProperty property()
	{
		return this.prop;
	}
}
