package com.zeitheron.portalstuff.api.tile.ctx.builtin.bool;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.utils.color.ColorARGB;
import com.zeitheron.portalstuff.Info;
import com.zeitheron.portalstuff.api.tile.ctx.ICTXRender;
import com.zeitheron.portalstuff.api.tile.ctx.IContextProperty;
import com.zeitheron.portalstuff.api.tile.ctx.IDataSender;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;

public class CTXBooleanRender implements ICTXRender
{
	public final ContextPropertyBoolean prop;
	public IDataSender sender;
	
	public CTXBooleanRender(ContextPropertyBoolean prop)
	{
		this.prop = prop;
	}
	
	@Override
	public int width()
	{
		return Minecraft.getMinecraft().fontRenderer.getStringWidth(I18n.format("context.boolean." + prop.name()) + ": " + prop.value) + 4;
	}
	
	@Override
	public int height()
	{
		return 25;
	}
	
	@Override
	public void mouseClicked(int mouseX, int mouseY)
	{
		this.prop.value = !this.prop.value;
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setBoolean("v", this.prop.value);
		this.sender.sendData(this.prop, nbt);
		HammerCore.audioProxy.playSoundAt((World) Minecraft.getMinecraft().world, Info.MOD_ID + ":ctx_editor.toggle", Minecraft.getMinecraft().player.getPosition(), 1.0f, 1.0f, SoundCategory.MASTER);
	}
	
	@Override
	public void render(int mouseX, int mouseY)
	{
		int w = this.width();
		int h = this.height();
		boolean hovered = mouseX >= 0 && mouseX < w && mouseY >= 0 && mouseY < h;
		GlStateManager.disableTexture2D();
		ColorARGB.glColourRGB((int) (hovered ? (this.prop.value ? 13406240 : 2132684) : (this.prop.value ? 16757570 : 4369919)));
		RenderUtil.drawTexturedModalRect((double) 0.0, (double) 0.0, (double) 0.0, (double) 0.0, (double) w, (double) h);
		GlStateManager.enableTexture2D();
		Minecraft.getMinecraft().fontRenderer.drawString(I18n.format("context.boolean." + prop.name()) + ": " + prop.value, 2, h / 2 - 4, 0);
	}
	
	@Override
	public void setSender(IDataSender sender)
	{
		this.sender = sender;
	}
	
	@Override
	public IContextProperty property()
	{
		return this.prop;
	}
}
