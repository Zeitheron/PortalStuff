package com.zeitheron.portalstuff.api.tile.ctx;

import net.minecraft.nbt.NBTTagCompound;

public interface IDataReceiver
{
	public void receiveData(NBTTagCompound var1);
}