package com.zeitheron.portalstuff.api.entity;

public interface ICube
{
	public EnumCubeType getCubeType();
	
	public static enum EnumCubeType
	{
		NORMAL, REFLECTION, COMPANION, WHEATLY, SPHERE;
	}
}