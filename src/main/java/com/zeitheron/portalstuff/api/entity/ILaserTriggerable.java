package com.zeitheron.portalstuff.api.entity;

public interface ILaserTriggerable
{
	public void toggle(boolean var1);
}