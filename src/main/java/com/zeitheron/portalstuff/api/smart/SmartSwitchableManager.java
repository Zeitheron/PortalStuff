package com.zeitheron.portalstuff.api.smart;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.hammercore.api.multipart.MultipartSignature;
import com.zeitheron.hammercore.internal.blocks.multipart.TileMultipart;
import com.zeitheron.hammercore.utils.WorldLocation;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.portalstuff.multipart.MultipartSmartWire;
import com.zeitheron.portalstuff.virtual.portalplacers.PortalPlacer;
import com.zeitheron.portalstuff.virtual.portalplacers.PortalPlacerList;
import com.zeitheron.portalstuff.virtual.portalplacers.PortalPlacerListManager;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class SmartSwitchableManager
{
	public static final List<IObtainer> OBTAINERS = new ArrayList<IObtainer>();
	
	public static void registerSwitchableObtainer(IObtainer o)
	{
		OBTAINERS.add(o);
	}
	
	public static void getSwitchablesAt(WorldLocation loc, EnumFacing handlerFace, List<ISmartSwitchable> switchables)
	{
		for(IObtainer f : OBTAINERS)
			f.obtain(loc, switchables, handlerFace);
	}
	
	public static void obtainSwitchables(List<ISmartSwitchable> switchables, World world, BlockPos obtainPos, EnumFacing handlerFace)
	{
		SmartSwitchableManager.obtainSwitchables(switchables, new WorldLocation(world, obtainPos), handlerFace);
	}
	
	public static void obtainSwitchables(List<ISmartSwitchable> switchables, WorldLocation obtainLoc, EnumFacing handlerFace)
	{
		SmartSwitchableManager.getSwitchablesAt(obtainLoc, handlerFace, switchables);
		for(EnumFacing face : EnumFacing.VALUES)
		{
			if(face.getAxis() == handlerFace.getAxis())
				continue;
			WorldLocation l2 = obtainLoc.offset(face);
			IBlockState state = l2.getState();
			World world = l2.getWorld();
			BlockPos pos = l2.getPos();
			Block bl = state.getBlock();
			if(face != null && (bl.isSideSolid(state, (IBlockAccess) world, pos, face.getOpposite()) || bl.isSideSolid(state, (IBlockAccess) world, pos, handlerFace.getOpposite())))
				continue;
			SmartSwitchableManager.getSwitchablesAt(l2, face, switchables);
		}
	}
	
	static
	{
		SmartSwitchableManager.registerSwitchableObtainer((loc, list, handlerFace) ->
		{
			ISmartSwitchable tileSwitchable = WorldUtil.cast(loc.getTile(), ISmartSwitchable.class);
			if(tileSwitchable != null)
				list.add(tileSwitchable);
		});
		
		SmartSwitchableManager.registerSwitchableObtainer((loc, list, handlerFace) ->
		{
			TileMultipart tmp = loc.getTileOfType(TileMultipart.class);
			if(tmp != null)
			{
				List<MultipartSignature> sings = tmp.signatures();
				for(MultipartSignature s : sings)
				{
					if(!(s instanceof MultipartSmartWire))
						continue;
					MultipartSmartWire wire = (MultipartSmartWire) s;
					if(wire.placedFace == handlerFace)
						continue;
					list.add(wire);
				}
			}
		});
		
		SmartSwitchableManager.registerSwitchableObtainer((loc, list, handlerFace) ->
		{
			PortalPlacerList data = PortalPlacerListManager.getPlacersListFor(loc.getWorld());
			PortalPlacer p = data.getPlacer(loc.getPos());
			if(p != null)
				list.add(p);
		});
	}
}