package com.zeitheron.portalstuff.api.smart;

import com.zeitheron.hammercore.api.handlers.ITileHandler;

import net.minecraft.block.properties.PropertyBool;

public interface ISmartSwitchable extends ITileHandler
{
	public static final PropertyBool ACTIVE = PropertyBool.create("active");
	
	public void setElementActive(boolean var1);
	
	public boolean isElementActive();
	
	default public boolean canReceive()
	{
		return true;
	}
}