package com.zeitheron.portalstuff.api.smart;

import java.util.List;

import com.zeitheron.hammercore.utils.WorldLocation;

import net.minecraft.util.EnumFacing;

public interface IObtainer
{
	public void obtain(WorldLocation var1, List<ISmartSwitchable> var2, EnumFacing var3);
}