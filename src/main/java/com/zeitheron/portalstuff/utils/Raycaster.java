package com.zeitheron.portalstuff.utils;

import java.util.ArrayList;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import com.zeitheron.hammercore.utils.WorldLocation;

import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class Raycaster
{
	public static boolean hasBlockOfChoiceInTheWay(World world, Vec3d start, Vec3d end, Predicate<WorldLocation> choice)
	{
		for(int x = (int) start.x; x < end.x; ++x)
		{
			for(int y = (int) start.y; y < end.y; ++y)
			{
				for(int z = (int) start.z; z < end.z; ++z)
				{
					WorldLocation loc = new WorldLocation(world, new BlockPos(x, y, z));
					ArrayList<AxisAlignedBB> aabbs = new ArrayList();
					loc.getBlock().addCollisionBoxToList(loc.getState(), world, loc.getPos(), loc.getAABB(), aabbs, null, false);
					boolean found = false;
					for(AxisAlignedBB aabb : aabbs)
					{
						if(aabb.offset(loc.getPos()).calculateIntercept(start, end) == null)
							continue;
						found = true;
						break;
					}
					if(!found || !choice.test(loc))
						continue;
					return true;
				}
			}
		}
		return false;
	}
	
	@Nullable
	public static BlockPos getPosOfBlockOfChoiceInTheWay(World world, Vec3d start, Vec3d end, Predicate<WorldLocation> choice)
	{
		ArrayList<BlockPos> locations = new ArrayList<BlockPos>();
		for(int x = (int) start.x; x < end.x; ++x)
		{
			for(int y = (int) start.y; y < end.y; ++y)
			{
				for(int z = (int) start.z; z < end.z; ++z)
				{
					WorldLocation loc = new WorldLocation(world, new BlockPos(x, y, z));
					ArrayList<AxisAlignedBB> aabbs = new ArrayList();
					loc.getBlock().addCollisionBoxToList(loc.getState(), world, loc.getPos(), loc.getAABB(), aabbs, null, false);
					boolean found = false;
					for(AxisAlignedBB aabb : aabbs)
					{
						if(aabb.offset(loc.getPos()).calculateIntercept(start, end) == null)
							continue;
						found = true;
						break;
					}
					if(!found || !choice.test(loc))
						continue;
					locations.add(loc.getPos());
				}
			}
		}
		if(!locations.isEmpty())
		{
			double dist = Double.POSITIVE_INFINITY;
			double buf = 0.0;
			BlockPos closest = null;
			for(BlockPos p : locations)
			{
				buf = p.distanceSqToCenter(start.x, start.y, start.z);
				if(buf >= dist || (dist = buf) == Double.POSITIVE_INFINITY)
					continue;
				closest = p;
			}
			return closest;
		}
		return null;
	}
}