package com.zeitheron.portalstuff.events;

import java.util.List;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.annotations.MCFBus;
import com.zeitheron.hammercore.event.WrenchEvent;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.VecDir;
import com.zeitheron.hammercore.utils.WorldLocation;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.portalstuff.PortalStuff;
import com.zeitheron.portalstuff.api.tile.ctx.IContextTile;
import com.zeitheron.portalstuff.cfg.ConfigsPS;
import com.zeitheron.portalstuff.init.BlocksPS;
import com.zeitheron.portalstuff.net.PacketRedifyFizzlers;
import com.zeitheron.portalstuff.virtual.portalplacers.PortalPlacer;
import com.zeitheron.portalstuff.virtual.portalplacers.PortalPlacerList;
import com.zeitheron.portalstuff.virtual.portalplacers.PortalPlacerListManager;

import me.ichun.mods.portalgun.common.entity.EntityPortalProjectile;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.ContainerPlayer;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.network.internal.FMLNetworkHandler;

@MCFBus
public class TickEvents
{
	@SubscribeEvent
	public void tickEnt(TickEvent.WorldTickEvent e)
	{
		PortalPlacerList list = PortalPlacerListManager.getPlacersListFor(e.world);
		if(e.phase == TickEvent.Phase.END)
		{
			list.setWorld(e.world);
		}
		List<EntityPortalProjectile> portalBalls = e.world.getEntities(EntityPortalProjectile.class, proj -> true);
		block2: for(EntityPortalProjectile proj2 : portalBalls)
		{
			VecDir vec = new VecDir(proj2.getPositionVector(), new Vec3d(proj2.motionX, proj2.motionY, proj2.motionZ), 8.0);
			RayTraceResult rtl;
			IBlockState is;
			Block block;
			if(ConfigsPS.placement_placementNeverFails || (rtl = e.world.rayTraceBlocks(vec.start, vec.calcEndVec(), true, false, false)) == null)
				continue;
			BlockPos hitPos = rtl.getBlockPos();
			Block bl = e.world.getBlockState(hitPos).getBlock();
			int meta = bl.getMetaFromState(e.world.getBlockState(hitPos));
			if(hitPos == null || (block = (is = e.world.getBlockState(hitPos)).getBlock()) == Blocks.IRON_BARS || block == BlocksPS.LASER_FIELD || block == BlocksPS.LASER_FIZZLER)
				continue;
			String name = is.toString();
			String[] massive = new String[] { name, bl.getRegistryName().toString(), ":" + meta };
			massive[2] = massive[1] + massive[2];
			for(String str : ConfigsPS.placement_placementList)
			{
				String[] variants = new String[] { str, new ResourceLocation(str).toString() };
				boolean match = false;
				block4: for(String state : variants)
				{
					for(String m : massive)
					{
						try
						{
							String a = state;
							String b = m;
							if(!a.equalsIgnoreCase(b))
								continue;
							match = true;
							break block4;
						} catch(Throwable a)
						{
							// empty catch block
						}
					}
				}
				if(match)
					continue block2;
			}
			proj2.setDead();
			if(proj2.shooter != null)
				HammerCore.audioProxy.playSoundAt(e.world, "portalgun:portalgun.portal_invalid_surface_swt", proj2.shooter.getPosition(), 0.5f, 1.0f, SoundCategory.MASTER);
			for(int i = 0; i < (bl == BlocksPS.FIZZLER_PART || bl == BlocksPS.FIZZLER ? 1 : 8); ++i)
				HCNet.spawnParticle(e.world, bl == BlocksPS.FIZZLER_PART || bl == BlocksPS.FIZZLER ? EnumParticleTypes.CRIT_MAGIC : EnumParticleTypes.BLOCK_CRACK, rtl.hitVec.x, rtl.hitVec.y, rtl.hitVec.z, 0, 0, 0, Block.getIdFromBlock(bl), meta);
			if(bl != BlocksPS.FIZZLER_PART && bl != BlocksPS.FIZZLER)
				continue;
			HCNet.INSTANCE.sendToAllAround(new PacketRedifyFizzlers(300), new WorldLocation(e.world, proj2.shooter.getPosition()).getPointWithRad(64));
		}
	}
	
	@SubscribeEvent
	public void wrenched(WrenchEvent e)
	{
		WorldLocation loc = new WorldLocation(e.player.world, e.pos);
		PortalPlacer placer = PortalPlacerListManager.getPlacersListFor(e.player.world).getPlacer(e.pos.offset(e.facing));
		if(!e.player.world.isRemote && placer != null && (e.player.openContainer == null || e.player.openContainer instanceof ContainerPlayer))
		{
			FMLNetworkHandler.openGui((EntityPlayer) e.player, (Object) PortalStuff.instance, (int) 1, (World) loc.getWorld(), (int) placer.positions[0].getX(), (int) placer.positions[0].getY(), (int) placer.positions[0].getZ());
			HCNet.swingArm((EntityPlayer) e.player, (EnumHand) e.hand);
		}
		IContextTile tile = (IContextTile) WorldUtil.cast((Object) loc.getTile(), IContextTile.class);
		if(!loc.getWorld().isRemote && tile != null)
		{
			TileSyncable sync = (TileSyncable) loc.getTileOfType(TileSyncable.class);
			if(sync != null)
			{
				sync.sync();
			}
			FMLNetworkHandler.openGui((EntityPlayer) e.player, (Object) PortalStuff.instance, (int) 0, (World) loc.getWorld(), (int) e.pos.getX(), (int) e.pos.getY(), (int) e.pos.getZ());
		}
	}
	
	@SubscribeEvent
	public void playerTick(TickEvent.PlayerTickEvent e)
	{
		if(e.phase == TickEvent.Phase.END)
		{
			return;
		}
		try
		{
			if(!(e.player.world.isRemote || !(e.player instanceof EntityPlayerMP) || e.player.ticksExisted % 40 != 0 && e.player.getPosition().equals((Object) new BlockPos(e.player.lastTickPosX, e.player.lastTickPosY, e.player.lastTickPosZ))))
			{
				PortalPlacerListManager.sendVisiblePlacersToPlayer((EntityPlayerMP) e.player);
			}
		} catch(Throwable throwable)
		{
			// empty catch block
		}
	}
}
