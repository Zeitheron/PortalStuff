package com.zeitheron.portalstuff.events;

import com.zeitheron.portalstuff.tile.TileLightBridge;

public class LightBridgeObtainer
{
	public static final ThreadLocal<TileLightBridge> BRIDGE = new ThreadLocal();
	
	public static TileLightBridge pull()
	{
		TileLightBridge b = BRIDGE.get();
		if(b != null)
			BRIDGE.set(null);
		return b;
	}
	
	public static void push(TileLightBridge bridge)
	{
		BRIDGE.set(bridge);
	}
}
