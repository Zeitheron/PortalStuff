package com.zeitheron.portalstuff;

public class Info
{
	public static final String MOD_ID = "portalstuff";
	public static final String MOD_NAME = "Portal Stuff";
	public static final String MOD_VERSION = "@VERSION@";
	public static final String MOD_BASE_PATH = "com.zeitheron." + MOD_ID;
}