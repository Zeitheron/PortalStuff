#version 120

uniform float time;
uniform float redify;
uniform sampler2D texture;
uniform float seed;

vec2 hash(in vec2 p)
{
    return cos(time + sin(mat2(17., 5., 3., 257.) * p - p) * 1234.5678 + seed);
}

float random (in vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))
                 * 43758.5453123);
}

float noise( in vec2 p )
{
	const float K1	= (sqrt(3.)-1.)/2.;
	const float K2	= (3.-sqrt(3.))/6.;
	
	vec2 i 		= floor(p + (p.x + p.y) * K1);
	
	vec2 a 		= p - i + (i.x + i.y)*K2;
	vec2 o 		= (a.x > a.y) ? vec2(1., 0.) : vec2(0., 1.);
	vec2 b 		= a - o + K2;
	vec2 c 		= a - 1.0 + 2.0 * K2;
	vec3 h 		= (.5 - vec3(dot(a, a), dot(b, b), dot(c, c))) * 3.;
	vec3 n 		= vec3(dot(a, hash(i)), dot(b, hash(i + o)), dot(c, hash(i+1.0)));
	
	return dot(n, h * h * h * h * h) * .5 + .5;
}

float noiseex (in vec2 st) {
    vec2 i = floor(st);
    vec2 f = fract(st);

    // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + vec2(1.0, 0.0));
    float c = random(i + vec2(0.0, 1.0));
    float d = random(i + vec2(1.0, 1.0));

    // Smooth Interpolation

    // Cubic Hermine Curve.  Same as SmoothStep()
    vec2 u = f*f*(3.0-2.0*f);
    // u = smoothstep(0.,1.,f);

    // Mix 4 coorners percentages
    return mix(a, b, u.x) +
            (c - a)* u.y * (1.0 - u.x) +
            (d - b) * u.x * u.y;
}

void main()
{
	vec3 color = vec3(0, .75, 1);
	vec3 fragTest = vec3(gl_TexCoord[0]);
    float colorMod = 1.05;
	vec2 p 	= gl_FragCoord.xy / 99999.;
	p = 2. * p - 1.;
	p += 10.;
	p *= .25;
	p += fragTest.xy * 100.;
	float f = 2.;
	float a = .5;
	float n = 0.2;
	for(int i = 0; i < 8; i++)
	{
		n += noise(p * f + noise(p * f) * .5) * a;
		p = p.yx;
		f *= 2.;
		a *= .5;
	}
	color = color * colorMod * n;
	gl_FragColor = vec4(color, redify);
}