#version 120

precision highp float;
uniform float time;
uniform vec2 resolution;
uniform float offset;

varying vec3 position;

#define PI 3.14159265359

float plot(vec2 st, float pct)
{
  return smoothstep(pct - 0.02, pct, st.y) - smoothstep(pct, pct + 0.02, st.y);
}

void main()
{
  vec2 st = gl_TexCoord[0].xy / resolution;
  float y = (sin(st.x * PI * 3. + time / 1.5) + 1.) / 4.3 + .25;
  
  vec3 color = vec3(1., 0.5, 1.);
  
  float pct = plot(st, offset + y);
  color = (1.0 - pct) * color + pct * vec3(0.0, .8, 1.0);
  
  pct = plot(st, offset + (cos(st.x * PI * 3. + (time - 20.5) / 1.55) + 1.) / 4.3 + .25);
  color = (1.0 - pct) * color + pct * vec3(0.0, .8, 1.0);
  
  pct = plot(st, offset + (cos(st.x * PI * 3. + (time - 37.) / 1.4) + 1.) / 4.3 + .25);
  color = (1.0 - pct) * color + pct * vec3(0.0, .8, 1.0);
  
  pct = plot(st, offset + ((sin(time * 1.1) + 1.) / 2.) * .465 + .25);
  color = (1.0 - pct) * color + pct * vec3(0.5, 0.7, 1.0);
  
  float alpha = 1. - color.r;
  color *= alpha;
  color.r = 0.;
  gl_FragColor = vec4(color, alpha);
}